package ru.hnau.folderplayer.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManagerUserAction
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager


class WidgetWideProvider : AppWidgetProvider() {

    companion object {

        private val UPDATE_ACTION = "UPDATE_WIDGETS"

        fun init() {
            PlayerManager.addOnStateChangedListener { updateWidgets() }
        }

        fun updateWidgets() {
            val context = ContextContainer.context
            val intent = Intent(context, WidgetWideProvider::class.java)
            intent.action = UPDATE_ACTION
            context.sendBroadcast(intent)
        }

    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        val state = PlayerManager.state
        val name = state.currentItem?.filename
        val paused = state.paused

        val views = getViews(context, name, paused)
        appWidgetIds.forEach { appWidgetManager.updateAppWidget(it, views) }
    }

    private fun getViews(context: Context, name: String?, paused: Boolean): RemoteViews {
        if (name == null) {
            return getInactiveViews(context)
        }
        return getActiveViews(context, name, paused)
    }

    private fun getInactiveViews(context: Context): RemoteViews {
        val widgetView = RemoteViews(context.packageName, R.layout.widget_wide_inactive)
        if (PreferencesManager.resumeWhereStoppedInactiveWidget) {
            PlayerSessionManagerUserAction.PAUSE_OR_RESUME.addToView(context, widgetView, R.id.container)
        } else {
            PlayerSessionManagerUserAction.SHOW_ACTIVITY.addToView(context, widgetView, R.id.container)
        }
        return widgetView
    }

    private fun getActiveViews(context: Context, name: String, paused: Boolean): RemoteViews {
        val widgetView = RemoteViews(context.packageName, R.layout.widget_wide)
        widgetView.setTextViewText(R.id.title, name)
        widgetView.setImageViewResource(R.id.pause_or_resume, if (paused) R.drawable.ic_media_play else R.drawable.ic_media_pause)

        PlayerSessionManagerUserAction.PREV.addToView(context, widgetView, R.id.prev)
        PlayerSessionManagerUserAction.PAUSE_OR_RESUME.addToView(context, widgetView, R.id.pause_or_resume)
        PlayerSessionManagerUserAction.NEXT.addToView(context, widgetView, R.id.next)
        PlayerSessionManagerUserAction.STOP.addToView(context, widgetView, R.id.stop)
        PlayerSessionManagerUserAction.SHOW_ACTIVITY.addToView(context, widgetView, R.id.title)

        return widgetView
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        if (intent.action != UPDATE_ACTION) {
            return
        }
        val thisAppWidget = ComponentName(context.packageName, WidgetWideProvider::class.java.name)
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val ids = appWidgetManager.getAppWidgetIds(thisAppWidget)
        onUpdate(context, appWidgetManager, ids)
    }

}
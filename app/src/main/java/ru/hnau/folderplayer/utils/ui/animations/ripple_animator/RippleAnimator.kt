package ru.hnau.folderplayer.utils.ui.animations.ripple_animator

import android.content.Context
import android.graphics.PointF
import android.view.MotionEvent
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.ui.animations.AnimatorsMetronome
import ru.hnau.folderplayer.utils.ui.animations.StatesAnimator


class RippleAnimator(
        private val context: Context,
        private val onNeedRefresh: () -> Unit,
        private val fingerCircleRadius: SizeGetter = SizeGetter(40),
        private val circleDeltaRadius: SizeGetter = SizeGetter(100),
        positionToTargetInterpolator: Interpolator = DEFAULT_POSITION_TO_TARGET_INTERPOLATOR,
        private val maxBgPercentage: Float = DEFAULT_MAX_BG_PERCENTAGE
) {

    companion object {
        private val DEFAULT_MAX_BG_PERCENTAGE = 0.65f
        private val MAX_CIRCLES_COUNT = 30
        private val CIRCLE_LIFETIME = 500L
        private val PRESS_TIME = 200.0

        private val DEFAULT_POSITION_TO_TARGET_INTERPOLATOR = DecelerateInterpolator()
    }

    private var downTime: Double? = null

    private val pressAnimator = StatesAnimator(
            statesCount = 2,
            stepListener = {
                bgPercentage = it * maxBgPercentage
                onNeedRefresh.invoke()
            },
            neighboringStatesAnimationTime = PRESS_TIME.toLong())

    var bgPercentage: Float = 0f
        private set

    private var fingerCirclePos: PointF? = null

    private val fingerCircleR: Float
        get() {
            val downTime = downTime ?: return 0f
            val now = System.currentTimeMillis().toDouble()
            val percentageRaw = (now - downTime) / PRESS_TIME
            val percentage = if (percentageRaw > 1) 1f else percentageRaw.toFloat()
            return percentage * fingerCircleRadius.get(context)
        }

    private val circles = (0 until MAX_CIRCLES_COUNT).map {
        RippleAnimatorCircle(circleDeltaRadius.get(context), CIRCLE_LIFETIME, positionToTargetInterpolator)
    }

    private val metronomeTicListener = this::callOnNeedRefresh

    val pressedPercentage: Float
        get() = pressAnimator.current.toFloat()

    fun onTouchEvent(event: MotionEvent, upTarget: PointF? = null) {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> onDown(event.x, event.y)
            MotionEvent.ACTION_MOVE -> onMove(event.x, event.y)
            else -> onUp(upTarget)
        }
    }

    private fun onDown(x: Float, y: Float) {
        downTime = System.currentTimeMillis().toDouble()
        fingerCirclePos = PointF(x, y)
        pressAnimator.addTarget(1, true)
    }

    private fun onMove(x: Float, y: Float) {
        fingerCirclePos = PointF(x, y)
        onNeedRefresh.invoke()
    }

    private fun onUp(upTarget: PointF?) {
        val fingerCircleR = this.fingerCircleR
        downTime = null
        val fingerCirclePos = this.fingerCirclePos ?: return
        this.fingerCirclePos = null
        addCircle(fingerCirclePos, fingerCircleR, upTarget)
        pressAnimator.addTarget(0,true)
    }

    private fun addCircle(initialPos: PointF, initialR: Float, upTarget: PointF?) {
        circles.forEach {
            if (it.finished) {
                it.start(initialPos, initialR, upTarget)
                AnimatorsMetronome.addListener(metronomeTicListener)
                return
            }
        }
    }

    private fun callOnNeedRefresh() {
        val firstActiveCircle = circles.find { !it.finished }
        if (firstActiveCircle == null) {
            AnimatorsMetronome.removeListener(metronomeTicListener)
        }
        onNeedRefresh.invoke()
    }

    fun drawCircles(drawer: (pos: PointF, r: Float, alpha: Float, progress: Float) -> Unit) {
        circles.forEach { circle ->
            circle.info?.let { info ->
                drawer.invoke(info.pos, info.r, info.alpha, info.progress)
            }
        }

        val fingerCirclePos = this.fingerCirclePos
        if (fingerCirclePos != null) {
            drawer.invoke(fingerCirclePos, fingerCircleR, 1f, 0f)
        }
    }


}
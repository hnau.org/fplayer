package ru.hnau.folderplayer.utils.getters

import android.content.Context


class StringGetter(resId: Int?, data: String? = null) : ResIdContextGetter<String>(resId, data) {

    constructor(data: String = ""): this(null, data)

    override fun generate(context: Context, resId: Int?) = resId?.let { context.getString(it) } ?: ""

}
package ru.hnau.folderplayer.utils.managers.bookmarks

import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.utils.managers.db.entities_utils.BookmarkDbUtils


class BookmarksContainer {

    private val bookmarks = BookmarkDbUtils.getAll(ContextContainer.context).associate { Pair(it.id, it) }.toMutableMap()

    val ids: List<Long>
        get() = bookmarks.keys.toSortedSet().toList()

    fun add(path: String): Long {
        val newId = ids.max()?.plus(1) ?: 0L
        val newBookmark = Bookmark(
                id = newId,
                path = path
        )
        bookmarks[newId] = newBookmark
        BookmarkDbUtils.insert(ContextContainer.context, newBookmark)
        return newId
    }

    fun remove(id: Long) {
        BookmarkDbUtils.remove(ContextContainer.context, id)
        bookmarks.remove(id)
    }

    fun getPath(id: Long) = bookmarks[id]?.path ?: ""

}
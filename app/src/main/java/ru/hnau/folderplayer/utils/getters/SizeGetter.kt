package ru.hnau.folderplayer.utils.getters

import android.content.Context
import ru.hnau.folderplayer.utils.ui.dpToPx


class SizeGetter(private val dp: Float? = null, px: Float? = null) : ContextGetter<Float>(px) {

    constructor(dp: Int): this(dp.toFloat())

    override fun generate(context: Context) = dp?.let { dpToPx(it) } ?: 0f

    fun getInt(context: Context) = get(context).toInt()

}
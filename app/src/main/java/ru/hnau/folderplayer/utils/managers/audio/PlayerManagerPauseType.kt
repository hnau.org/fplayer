package ru.hnau.folderplayer.utils.managers.audio


enum class PlayerManagerPauseType(private val priority: Int) {

    USER(1),
    HEADPHONES(0),
    AUDIO_FOCUS(0);

    fun moreImportantOrEqualsThan(other: PlayerManagerPauseType) = this.priority >= other.priority

}
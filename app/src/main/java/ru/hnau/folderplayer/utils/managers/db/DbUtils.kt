package ru.hnau.folderplayer.utils.managers.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Parcel
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.managers.Logger
import java.util.*


object DbUtils {

    private val TAG = DbUtils::class.java.simpleName

    fun <T> queryOneWithSimpleWhere(
            context: Context,
            table: String,
            entityConstrictor: (Cursor) -> T,
            whereField: String,
            whereValue: Any
    ) = queryListWithSimpleWhere(context, table, entityConstrictor, whereField, whereValue, 1).firstOrNull()

    fun <T> queryOneWithAndsWhere(
            context: Context,
            table: String,
            entityConstrictor: (Cursor) -> T,
            whereFieldsWithValues: Map<String, Any?>
    ) = queryListWithAndsWhere(context, table, entityConstrictor, whereFieldsWithValues, 1).firstOrNull()

    fun <T> queryOne(
            context: Context,
            table: String,
            entityConstrictor: (Cursor) -> T,
            whereClause: String? = null,
            whereArgs: List<Any>? = null
    ) = queryList(context, table, entityConstrictor, whereClause, whereArgs, 1).firstOrNull()


    fun <T> queryListWithSimpleWhere(
            context: Context,
            table: String,
            entityConstrictor: (Cursor) -> T,
            whereField: String,
            whereValue: Any,
            limit: Int? = null,
            sortBy: String? = null,
            sortPositive: Boolean = true
    ) = queryListWithAndsWhere(context, table, entityConstrictor, hashMapOf(Pair(whereField, whereValue)), limit, sortBy, sortPositive)

    fun <T> queryListWithAndsWhere(
            context: Context,
            table: String,
            entityConstrictor: (Cursor) -> T,
            whereFieldsWithValues: Map<String, Any?>,
            limit: Int? = null,
            sortBy: String? = null,
            sortPositive: Boolean = true
    ): List<T> {
        val whereClause = getAndWhereClause(whereFieldsWithValues)
        return queryList(context, table, entityConstrictor, whereClause.first, whereClause.second, limit, sortBy, sortPositive)
    }

    fun <T> queryList(
            context: Context,
            table: String,
            entityConstrictor: (Cursor) -> T,
            whereClause: String? = null,
            whereArgs: List<Any>? = null,
            limit: Int? = null,
            sortBy: String? = null,
            sortPositive: Boolean = true
    ): List<T> {
        val db = DbManager.getDb(context) ?: return emptyList()

        val wherePart = whereClause?.let { " WHERE $it" } ?: ""
        val limitPart = limit?.let { " LIMIT $it" } ?: ""
        val sortPart = sortBy?.let { " ORDER BY $sortBy ${if (sortPositive) "ASC" else "DESC"}" } ?: ""
        val sql = "SELECT * FROM $table$wherePart$sortPart$limitPart"

        return Utils.tryOrNull {
            val result = ArrayList<T>()
            val args = genArgs(whereArgs)
            Logger.d(TAG, "Query by '$sql' with args ${argsToString(args)}")
            val cursor = db.rawQuery(sql, args)
            while (cursor.moveToNext()) {
                result.add(entityConstrictor.invoke(cursor))
            }
            cursor.close()
            result
        } ?: emptyList()
    }

    fun removeWithSimpleWhere(
            context: Context,
            table: String,
            whereField: String,
            whereValue: Any
    ) = removeWithAndsWhere(context, table, hashMapOf(Pair(whereField, whereValue)))

    fun removeWithAndsWhere(
            context: Context,
            table: String,
            whereFieldsWithValues: Map<String, Any?>
    ) {
        val whereClause = getAndWhereClause(whereFieldsWithValues)
        remove(context, table, whereClause.first, whereClause.second)
    }

    fun remove(
            context: Context,
            table: String,
            whereClause: String? = null,
            whereArgs: List<Any>? = null
    ) {
        val db = DbManager.getDb(context) ?: return
        Utils.tryCatch {
            val args = genArgs(whereArgs)
            Logger.d(TAG, "Removing by '$whereClause' by args '${argsToString(args)}'")
            db.delete(table, whereClause, args)
        }
    }

    fun queryCountWithSimpleWhere(
            context: Context,
            table: String,
            whereField: String,
            whereValue: Any
    ) = queryCountWithAndsWhere(context, table, hashMapOf(Pair(whereField, whereValue)))

    fun queryCountWithAndsWhere(
            context: Context,
            table: String,
            whereFieldsWithValues: Map<String, Any?>
    ): Long {
        val whereClause = getAndWhereClause(whereFieldsWithValues)
        return queryCount(context, table, whereClause.first, whereClause.second)
    }

    fun queryCount(
            context: Context,
            table: String,
            whereClause: String? = null,
            whereArgs: List<Any>? = null
    ): Long {
        val db = DbManager.getDb(context) ?: return 0L

        var whereClauseWithArgs = whereClause
        whereArgs?.forEach {
            whereClauseWithArgs = whereClauseWithArgs?.replace("?", valueToSqlValue(it))
        }
        val wherePart = whereClauseWithArgs?.let { " WHERE $it" } ?: ""
        val sql = "SELECT COUNT(*) FROM $table$wherePart"

        return Utils.tryOrNull {
            Logger.d(TAG, "Query from table $table count by '$sql'")
            val statement = db.compileStatement(sql)
            statement.simpleQueryForLong()
        } ?: 0L
    }

    fun updateWithSimpleWhere(
            context: Context,
            table: String,
            updateField: String,
            updateValue: Any,
            whereField: String,
            whereValue: Any
    ) = updateWithSimpleWhere(context, table, hashMapOf(Pair(updateField, updateValue)), whereField, whereValue)

    fun updateWithSimpleWhere(
            context: Context,
            table: String,
            fieldsWithValues: Map<String, Any?>,
            whereField: String,
            whereValue: Any
    ) = updateWithAndsWhere(context, table, fieldsWithValues, hashMapOf(Pair(whereField, whereValue)))

    fun updateWithAndsWhere(
            context: Context,
            table: String,
            updateField: String,
            updateValue: Any,
            whereFieldsWithValues: Map<String, Any?>
    ) = updateWithAndsWhere(context, table, hashMapOf(Pair(updateField, updateValue)), whereFieldsWithValues)

    fun updateWithAndsWhere(
            context: Context,
            table: String,
            fieldsWithValues: Map<String, Any?>,
            whereFieldsWithValues: Map<String, Any?>
    ): Int {
        val whereClause = getAndWhereClause(whereFieldsWithValues)
        return update(context, table, fieldsWithValues, whereClause.first, whereClause.second)
    }

    fun update(
            context: Context,
            table: String,
            updateField: String,
            updateValue: Any,
            whereClause: String? = null,
            whereArgs: List<Any>? = null
    ) = update(context, table, hashMapOf(Pair(updateField, updateValue)), whereClause, whereArgs)

    fun update(
            context: Context,
            table: String,
            fieldsWithValues: Map<String, Any?>,
            whereClause: String? = null,
            whereArgs: List<Any>? = null
    ): Int {
        val db = DbManager.getDb(context) ?: return 0

        val contentValues = hashMapToContentValues(fieldsWithValues)

        return Utils.tryOrNull {
            val args = genArgs(whereArgs)
            Logger.d(TAG, "Update in table $table '$contentValues' by where '$whereClause' with args ${argsToString(args)}")
            db.update(table, contentValues, whereClause, args)
        } ?: 0
    }

    fun insertOrUpdate(context: Context, table: String, fieldsWithValues: Map<String, Any?>): Boolean {
        val db = DbManager.getDb(context) ?: return false
        val contentValues = hashMapToContentValues(fieldsWithValues)
        Logger.d(TAG, "Insert or update in table $table '$contentValues'")
        return Utils.tryOrNull { db.insertWithOnConflict(table, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE) > -1 } ?: false
    }

    fun insert(context: Context, table: String, fieldsWithValues: Map<String, Any?>): Boolean {
        val db = DbManager.getDb(context) ?: return false

        val contentValues = hashMapToContentValues(fieldsWithValues)
        return Utils.tryOrElse({
            Logger.d(TAG, "Insert into table $table '$contentValues'")
            db.insert(table, null, contentValues) >= 0
        }, {
            false
        })
    }

    private fun valueToSqlValue(value: Any): String = when (value) {
        is String -> "'$value'"
        else -> value.toString()
    }

    private fun hashMapToContentValues(fieldsWithValues: Map<String, Any?>): ContentValues {
        val parcel = Parcel.obtain()
        parcel.writeMap(fieldsWithValues)
        parcel.setDataPosition(0)
        val contentValues = ContentValues.CREATOR.createFromParcel(parcel)
        parcel.recycle()
        return contentValues
    }

    private fun clearDb(context: Context) = Utils.tryCatch {
        DbUpdater.tablesNames.forEach { tableName ->
            remove(context, tableName)
        }
    }

    private fun genArgs(rawArgs: List<Any>?) = rawArgs?.map { it.toString() }?.toTypedArray() ?: emptyArray()

    private fun argsToString(args: Array<String>) = "[${args.joinToString()}]"

    private fun getAndWhereClause(whereFieldsWithValues: Map<String, Any?>) =
            whereFieldsWithValues.toList().let { fieldValuesList ->
                Pair(fieldValuesList.joinToString(",", transform = { "${it.first}=?" }), fieldValuesList.map { it.second ?: "" })
            }
}
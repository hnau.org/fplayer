package ru.hnau.folderplayer.utils.managers.audio.equalizer

import ru.hnau.folderplayer.utils.listeners_containers.ListenersDataContainer
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.audio.equalizer.wrapper.EqualizerWrapper
import ru.hnau.folderplayer.utils.managers.audio.equalizer.wrapper.RealEqualizerWrapper


object EqualizerManager {

    private val equalizerWrapper = EqualizerWrapper.create(PlayerManager.audioSessionId)

    private val onSchemeAddedListenersContainer = ListenersDataContainer<Long>()
    private val onSchemeRemovedListenersContainer = ListenersDataContainer<Pair<List<Long>, Long>>()
    private val onSchemeSelectedListenersContainer: ListenersDataContainer<Long> = ListenersDataContainer<Long>().apply {
        add(this@EqualizerManager::onSchemeSelected)
    }

    private val schemasContainer: SchemasContainer = SchemasContainer(
            onSchemeAddedListener = onSchemeAddedListenersContainer::call,
            onSchemeRemovedListener = onSchemeRemovedListenersContainer::call,
            onSchemeSelectedListener = onSchemeSelectedListenersContainer::call,
            onCurrentSchemeBandChangedListener = this::onCurrentSchemeBandChanged
    )

    var currentSchemeId: Long
        set(value) {
            schemasContainer.currentSchemeId = value
        }
        get() = schemasContainer.currentSchemeId

    val schemasIds: List<Long>
        get() = schemasContainer.ids

    val bandsCount: Short
        get() = equalizerWrapper.bandsCount

    init {
        onSchemeSelected(currentSchemeId)
    }

    fun startIfNeed() {}

    fun addOnSelectedSchemeAddedListener(listener: (Long) -> Unit) = onSchemeAddedListenersContainer.add(listener)
    fun removeOnSelectedSchemeAddedListener(listener: (Long) -> Unit) = onSchemeAddedListenersContainer.remove(listener)
    fun addOnSelectedSchemeRemovedListener(listener: (Pair<List<Long>, Long>) -> Unit) = onSchemeRemovedListenersContainer.add(listener)
    fun removeOnSelectedSchemeRemovedListener(listener: (Pair<List<Long>, Long>) -> Unit) = onSchemeRemovedListenersContainer.remove(listener)
    fun addOnSelectedSchemeSelectedListener(listener: (Long) -> Unit) = onSchemeSelectedListenersContainer.add(listener)
    fun removeOnSelectedSchemeSelectedListener(listener: (Long) -> Unit) = onSchemeSelectedListenersContainer.remove(listener)

    fun getBandFreq(bandNum: Int) = equalizerWrapper.getBandFreq(bandNum)
    fun addOnSchemeNameChangedListener(id: Long, listener: (String) -> Unit) = schemasContainer.addOnSchemeNameChangedListener(id, listener)
    fun removeOnSchemeNameChangedListener(id: Long, listener: (String) -> Unit) = schemasContainer.removeOnSchemeNameChangedListener(id, listener)
    fun addOnSchemeBandChangedListener(id: Long, listener: (Int) -> Unit) = schemasContainer.addOnSchemeBandChangedListener(id, listener)
    fun removeOnSchemeBandChangedListener(id: Long, listener: (Int) -> Unit) = schemasContainer.removeOnSchemeBandChangedListener(id, listener)
    fun getSchemeName(id: Long) = schemasContainer.getSchemeName(id)
    fun setSchemeName(id: Long, name: String) = schemasContainer.setSchemeName(id, name)
    fun getSchemeValue(id: Long, band: Int) = schemasContainer.getSchemeValue(id, band)
    fun setSchemeValue(id: Long, band: Int, value: Float) = schemasContainer.setSchemeValue(id, band, value)
    fun saveAllSchemas() = schemasContainer.saveAllSchemas()
    fun isSchemeEditable(id: Long) = schemasContainer.isSchemeEditable(id)

    fun duplicateScheme(id: Long) {
        schemasContainer.duplicateScheme(id)
    }

    fun removeScheme(id: Long) {
        schemasContainer.removeScheme(id)
    }

    private fun onSchemeSelected(schemeId: Long) = applyScheme(schemeId, schemasContainer)

    private fun onCurrentSchemeBandChanged(schemeId: Long, band: Int, value: Float) = applyScheme(schemeId, schemasContainer, band)

    private fun applyScheme(schemeId: Long, schemasContainer: SchemasContainer?, onlyThisBand: Int? = null) {
        schemasContainer ?: return
        equalizerWrapper.applyScheme(schemeId, schemasContainer, onlyThisBand)
    }

}
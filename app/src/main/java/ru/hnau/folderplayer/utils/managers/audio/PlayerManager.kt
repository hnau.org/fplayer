package ru.hnau.folderplayer.utils.managers.audio

import android.media.MediaPlayer
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.listeners_containers.ListenersContainer
import ru.hnau.folderplayer.utils.listeners_containers.ListenersDataContainer
import ru.hnau.folderplayer.utils.managers.AudioFocusManager
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.HeadphonesPluggedManager
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager


object PlayerManager : MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    private val QUIET_VOLUME = 0.5f

    data class State(
            val currentItem: PlayItem? = PlayerManager.currentItem,
            val paused: Boolean = PlayerManager.status != PlayerManager.Status.PLAY
    )

    enum class Status {
        STOP,
        PLAY,
        PAUSE
    }

    private val player = MediaPlayer().apply {
        isLooping = false
        setOnErrorListener(this@PlayerManager)
        setOnCompletionListener(this@PlayerManager)
        setOnPreparedListener { onPrepared() }
    }

    val audioSessionId = player.audioSessionId

    var currentItem: PlayItem? = null
        private set

    var status = Status.STOP
        private set

    var duration: Int = 0
        private set

    var position: Int
        set(value) {
            if (status == Status.STOP) {
                return
            }
            val position = if (value < 0) 0 else if (value > duration) duration else value
            player.seekTo(position)
        }
        get() {
            if (status == Status.STOP) {
                return 0
            }
            return player.currentPosition
        }

    var percentage: Float
        set(value) {
            position = (value * duration).toInt()
        }
        get() = if (duration <= 0) 0f else position.toFloat() / duration.toFloat()

    var state: State = State()
        private set(value) {
            if (field != value) {
                field = value
                onStateChangedListenersContainer.call(value)
            }
        }

    var quiet = false
        set(value) {
            if (field != value) {
                field = value
                onQuietChanged(value)
            }
        }

    private val onErrorListenersContainer = ListenersContainer()
    private val onFinishedListenersContainer = ListenersContainer()

    private val onStateChangedListenersContainer = ListenersDataContainer<State>()

    private var pausedType: PlayerManagerPauseType? = null

    private val headphonesPluggedStateChangedListener = { plugged: Boolean ->
        if (plugged) {
            if (PreferencesManager.resumeOnHeadphonesPlugged) {
                resume(PlayerManagerPauseType.HEADPHONES)
            }
        } else {
            if (PreferencesManager.pauseOnHeadphonesUnplugged) {
                pause(PlayerManagerPauseType.HEADPHONES)
            }
        }
    }

    private val audioFocusStateChangedListener = { focused: Boolean ->
        if (focused) {
            if (PreferencesManager.resumeOnFocused) {
                resume(PlayerManagerPauseType.AUDIO_FOCUS)
            }
        } else {
            if (PreferencesManager.pauseOnUnfocused) {
                pause(PlayerManagerPauseType.AUDIO_FOCUS)
            }
        }
    }

    fun addOnStateChangedListener(listener: (State) -> Unit) = onStateChangedListenersContainer.add(listener)
    fun removeOnStateChangedListener(listener: (State) -> Unit) = onStateChangedListenersContainer.remove(listener)

    fun addOnErrorListener(listener: () -> Unit) = onErrorListenersContainer.add(listener)
    fun removeOnErrorListener(listener: () -> Unit) = onErrorListenersContainer.remove(listener)

    fun addOnFinishedListener(listener: () -> Unit) = onFinishedListenersContainer.add(listener)
    fun removeOnFinishedListener(listener: () -> Unit) = onFinishedListenersContainer.remove(listener)

    private var firstItem = false

    private fun onQuietChanged(quiet: Boolean) {
        val volume = if (quiet) QUIET_VOLUME else 1f
        player.setVolume(volume, volume)
    }

    fun start(item: PlayItem, first: Boolean): Boolean {
        EqualizerManager.startIfNeed()
        return synchronized(this, {
            if (status != Status.STOP) {
                stopInner()
            }
            pausedType = null
            currentItem = item
            firstItem = first
            return@synchronized Utils.tryOrElse({
                player.setDataSource(item.path)
                player.prepareAsync()
                return@tryOrElse true
            }, {
                stopInner()
                onStateChanged()
                FirebaseManager.sendError("Unable to play file ${item.path}")
                return@tryOrElse false
            })
        })
    }

    private fun onPrepared() {
        val item = currentItem ?: return
        status = Status.PLAY
        player.start()
        duration = player.duration
        goToSavedOnStoppedSecondsPosition(item)
        onStateChanged()
    }

    private fun goToSavedOnStoppedSecondsPosition(item: PlayItem) {
        if (!firstItem) {
            return
        }
        firstItem = false
        val seconds = ResumeWhereStoppedManager.getPlayItemStartSecondsPosition(item) ?: return
        this.position = seconds * 1000
    }

    fun stop() {
        val currentItem = this.currentItem
        if (currentItem != null) {
            ResumeWhereStoppedManager.setPlayItemStartSecondsPosition(currentItem, position / 1000)
        }
        stopInner()
        onStateChanged()
    }

    private fun stopInner() {
        synchronized(this, {
            if (status == Status.STOP) {
                return
            }
            status = Status.STOP
            player.reset()
            onStopped()
        })
    }

    fun pauseOrResume(pauseType: PlayerManagerPauseType): Boolean {
        if (state.currentItem == null) {
            return false
        }
        if (state.paused) {
            resume(pauseType)
        } else {
            pause(pauseType)
        }
        return true
    }

    private fun resume(pauseType: PlayerManagerPauseType) {
        val pausedType = this.pausedType ?: return
        if (!pauseType.moreImportantOrEqualsThan(pausedType)) {
            return
        }
        this.pausedType = null
        resumeInner()
    }

    private fun pause(pauseType: PlayerManagerPauseType) {
        if (this.pausedType != null) {
            return
        }
        this.pausedType = pauseType
        pauseInner()
    }

    private fun pauseInner() {
        synchronized(this, {
            if (status != Status.PLAY) {
                return
            }
            status = Status.PAUSE
            player.pause()
        })
        onStateChanged()
    }

    private fun resumeInner() {
        synchronized(this, {
            if (status != Status.PAUSE) {
                return
            }
            status = Status.PLAY
            player.start()
        })
        onStateChanged()
    }

    private fun onStateChanged() {
        val state = State()
        this.state = state

        if (state.currentItem == null) {
            HeadphonesPluggedManager.removeOnPluggedStateChangedListener(headphonesPluggedStateChangedListener)
            AudioFocusManager.removeOnStateChangedListener(audioFocusStateChangedListener)
        } else {
            HeadphonesPluggedManager.addOnPluggedStateChangedListener(headphonesPluggedStateChangedListener)
            AudioFocusManager.addOnStateChangedListener(audioFocusStateChangedListener)
        }
    }

    private fun onStopped() {
        duration = 0
        currentItem = null
    }

    override fun onCompletion(mp: MediaPlayer?) {
        synchronized(this, {
            stopInner()
        })
        onFinishedListenersContainer.call()
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        synchronized(this, {
            stopInner()
        })
        onErrorListenersContainer.call()
        onStateChanged()
        return true
    }

}
package ru.hnau.folderplayer.utils.ui.animations

import android.os.Handler
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.listeners_containers.ListenersContainer
import ru.hnau.folderplayer.utils.managers.Logger
import kotlin.concurrent.thread

/**
 * Вызов слушателей для отрисовки кадров анимации
 */
object AnimatorsMetronome {
    private val TAG = AnimatorsMetronome::class.java.simpleName

    private val listeners = ListenersContainer()

    private val uiHandler = Handler()

    init {
        thread(start = true, isDaemon = true, block = {
            while (true) {
                if (!listeners.empty()) {
                    uiHandler.post { listeners.call() }
                }
                Utils.tryCatch { Thread.sleep(AnimationManager.ANIMATION_STEP_TIME) }
            }
        })
    }

    fun addListener(listener: () -> Unit) {
        listeners.add(listener)
        Logger.d(TAG, "+++ [${listeners.count()}]")
    }

    fun removeListener(listener: () -> Unit) {
        listeners.remove(listener)
        Logger.d(TAG, "--- [${listeners.count()}]")
    }


}
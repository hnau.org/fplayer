package ru.hnau.folderplayer.utils.managers.db.entities_utils

import android.content.Context
import android.database.Cursor
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerScheme
import ru.hnau.folderplayer.utils.managers.db.DbUtils
import ru.hnau.folderplayer.utils.toBoolean
import ru.hnau.folderplayer.utils.toInt


object EqualizerSchemeDbUtils {

    private val TABLE_NAME = "EQUALIZER_SCHEME"

    private val FIELD_NAME_ID = "ID"
    private val FIELD_NAME_EDITABLE = "EDITABLE"
    private val FIELD_NAME_NAME = "NAME"
    private val FIELD_NAME_VALUES = "BANDS_VALUES"

    private val constructorFromCursor = { cursor: Cursor ->
        EqualizerScheme(
                id = cursor.getLong(cursor.getColumnIndex(FIELD_NAME_ID)),
                editable = cursor.getInt(cursor.getColumnIndex(FIELD_NAME_EDITABLE)).toBoolean(),
                name = cursor.getString(cursor.getColumnIndex(FIELD_NAME_NAME)),
                valuesString = cursor.getString(cursor.getColumnIndex(FIELD_NAME_VALUES))
        )
    }

    fun getCount(context: Context?) = context?.let {
        DbUtils.queryCount(
                context = it,
                table = TABLE_NAME
        )
    } ?: 0

    fun insertAll(context: Context?, schemas: List<EqualizerScheme>) = schemas.forEach { insert(context, it) }

    fun getAll(context: Context?) = context?.let {
        DbUtils.queryList(
                context = it,
                table = TABLE_NAME,
                entityConstrictor = constructorFromCursor
        )
    } ?: emptyList()

    fun insert(context: Context?, scheme: EqualizerScheme) = context?.let {
        DbUtils.insert(it, TABLE_NAME, hashMapOf(
                Pair(FIELD_NAME_ID, scheme.id),
                Pair(FIELD_NAME_EDITABLE, scheme.editable.toInt()),
                Pair(FIELD_NAME_NAME, scheme.name),
                Pair(FIELD_NAME_VALUES, scheme.getValuesString())
        ))
    }

    fun remove(context: Context?, id: Long) = context?.let {
        DbUtils.removeWithSimpleWhere(it, TABLE_NAME, FIELD_NAME_ID, id)
    }

    fun update(context: Context?, scheme: EqualizerScheme) = context?.let {
        DbUtils.updateWithSimpleWhere(
                it,
                TABLE_NAME,
                hashMapOf(
                        FIELD_NAME_EDITABLE to scheme.editable.toInt(),
                        FIELD_NAME_NAME to scheme.name,
                        FIELD_NAME_VALUES to scheme.getValuesString()
                ),
                FIELD_NAME_ID,
                scheme.id)
    }


}
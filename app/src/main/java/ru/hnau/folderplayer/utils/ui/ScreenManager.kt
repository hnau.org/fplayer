package ru.hnau.folderplayer.utils.ui

import android.content.Context
import android.graphics.Point
import android.os.Build
import android.util.DisplayMetrics
import ru.hnau.folderplayer.utils.managers.ContextContainer


object ScreenManager {

    private var basePxDpK = 1f

    private val scaledPxDpK
        get() = basePxDpK / ScaleManager.value

    var sizePx = Point(720, 1280)
        private set

    val sizeDp: Point by lazy {
        Point(
                (sizePx.x * basePxDpK).toInt(),
                (sizePx.y * basePxDpK).toInt()
        )
    }

    var statusBarHeight: Int = 0
        private set

    init {
        val context = ContextContainer.context
        val displayMetrics = context.resources.displayMetrics
        val densityDpi = displayMetrics.densityDpi.toFloat()
        basePxDpK = DisplayMetrics.DENSITY_DEFAULT / densityDpi
        sizePx = Point(displayMetrics.widthPixels, displayMetrics.heightPixels)
        statusBarHeight = calcStatusBarHeightHeight(context)
    }

    internal fun pxToDp(px: Float) = px * scaledPxDpK

    internal fun dpToPx(dp: Float) = dp / scaledPxDpK


    private fun calcStatusBarHeightHeight(context: Context): Int {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return 0
        }

        val resources = context.resources
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId <= 0) {
            return 0
        }
        return resources.getDimensionPixelSize(resourceId)
    }


}

fun pxToDp(px: Float) = ScreenManager.pxToDp(px)

fun dpToPx(dp: Float) = ScreenManager.dpToPx(dp)

fun pxToDp(px: Int) = pxToDp(px.toFloat())

fun dpToPx(dp: Int) = dpToPx(dp.toFloat())

fun pxToDpInt(px: Float) = pxToDp(px).toInt()

fun dpToPxInt(dp: Float) = dpToPx(dp).toInt()

fun pxToDpInt(px: Int) = pxToDp(px).toInt()

fun dpToPxInt(dp: Int) = dpToPx(dp).toInt()
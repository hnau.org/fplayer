package ru.hnau.folderplayer.utils.managers.audio.equalizer.wrapper

import ru.hnau.folderplayer.utils.managers.audio.equalizer.SchemasContainer


class EmptyEqualizerWrapper private constructor() : EqualizerWrapper(0) {

    override fun getBandFreq(bandNum: Int) = 0

    override fun applyScheme(schemeId: Long, schemasContainer: SchemasContainer, onlyThisBand: Int?) {}

    companion object {
        val INSTANCE = EmptyEqualizerWrapper()
    }

}
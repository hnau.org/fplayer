package ru.hnau.folderplayer.utils.managers.audio.equalizer.wrapper

import android.media.audiofx.Equalizer
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.managers.audio.equalizer.SchemasContainer


abstract class EqualizerWrapper(val bandsCount: Short) {

    abstract fun getBandFreq(bandNum: Int): Int

    abstract fun applyScheme(schemeId: Long, schemasContainer: SchemasContainer, onlyThisBand: Int? = null)

    companion object {

        fun create(audioSessionId: Int): EqualizerWrapper {

            val equalizer = Utils.tryOrNull { Equalizer(0, audioSessionId) } ?: return EmptyEqualizerWrapper.INSTANCE
            equalizer.enabled = true

            val range: ShortArray? = equalizer.bandLevelRange
            val rangeMin = range?.getOrNull(0) ?: return EmptyEqualizerWrapper.INSTANCE
            val rangeMax = range.getOrNull(1) ?: return EmptyEqualizerWrapper.INSTANCE

            val bandsCount = equalizer.numberOfBands
            val bandsFreq = (0 until bandsCount).map { equalizer.getCenterFreq(it.toShort()) / 1000 }

            return RealEqualizerWrapper(
                    equalizer = equalizer,
                    bandsCount = bandsCount,
                    bandsFreq = bandsFreq,
                    rangeMin = rangeMin,
                    rangeMax = rangeMax
            )

        }

    }

}
package ru.hnau.folderplayer.utils.managers

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics


object FirebaseManager {

    private val analytics =
        FirebaseAnalytics.getInstance(ContextContainer.context)

    fun sendEvent(
        identifier: String,
        params: Map<String, String>? = null
    ) {
        analytics.logEvent(
            identifier,
            Bundle().apply {
                params?.forEach { (key, value) ->
                    putString(key, value)
                }
            }
        )
    }

    fun sendError(error: String) =
        FirebaseAnalytics.getInstance(ContextContainer.context).logEvent(error, null)

    fun sendThrowable(th: Throwable) =
        sendError("Throwable: ${th.message}")

}
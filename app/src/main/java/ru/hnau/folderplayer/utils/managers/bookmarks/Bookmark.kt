package ru.hnau.folderplayer.utils.managers.bookmarks


data class Bookmark(
        val id: Long,
        val path: String
)
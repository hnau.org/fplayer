package ru.hnau.folderplayer.utils.managers.audio

import android.content.Context
import ru.hnau.folderplayer.utils.files_tree.item.FilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.FilesTreeItem.Companion.isEquals
import ru.hnau.folderplayer.utils.files_tree.item.FsFileFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.FsFilesTreeItem
import ru.hnau.folderplayer.utils.listeners_containers.ListenersContainer
import ru.hnau.folderplayer.utils.managers.ContextContainer
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManagerStarter
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager


object ResumeWhereStoppedManager {

    private val onResumeWhereStoppedStateChangedListenersContainer = ListenersContainer()

    fun addResumeWhereStoppedStateChangedListener(listener: () -> Unit) = onResumeWhereStoppedStateChangedListenersContainer.add(listener)
    fun removeResumeWhereStoppedStateChangedListener(listener: () -> Unit) = onResumeWhereStoppedStateChangedListenersContainer.remove(listener)

    var resumeWhereStopped: Boolean
        set(value) {
            PreferencesManager.resumeWhereStopped = value
            onResumeWhereStoppedStateChangedListenersContainer.call()
        }
        get() = PreferencesManager.resumeWhereStopped

    private fun getFileStartSecondsPosition(path: String): Int? {
        if (!resumeWhereStopped) {
            return null
        }
        val preferencesPath = PreferencesManager.resumeWhereStoppedFilePath
        if (preferencesPath != path) {
            return null
        }

        return PreferencesManager.resumeWhereStoppedSeconds
    }

    fun getPlayItemStartSecondsPosition(playItem: PlayItem) = getFileStartSecondsPosition(playItem.path)

    fun getFilesTreeItemStartSecondsPosition(filesTreeItem: FilesTreeItem): Int? {
        if (filesTreeItem !is FsFileFilesTreeItem) {
            return null
        }
        return getFileStartSecondsPosition(filesTreeItem.path)
    }

    private fun setFileStartSecondsPosition(path: String, seconds: Int) {
        PreferencesManager.resumeWhereStoppedFilePath = path
        PreferencesManager.resumeWhereStoppedSeconds = seconds
        onResumeWhereStoppedStateChangedListenersContainer.call()
    }

    fun setPlayItemStartSecondsPosition(playItem: PlayItem, seconds: Int) = setFileStartSecondsPosition(playItem.path, seconds)

    fun tryResumeWhereStopped() {
        val path = PreferencesManager.resumeWhereStoppedFilePath
        val filesTreeItem = FsFilesTreeItem.createByPath(ContextContainer.context, path) as? FsFileFilesTreeItem ?: return
        if (!filesTreeItem.audio) {
            return
        }
        PlayerSessionManagerStarter.onAudioClicked(filesTreeItem)
    }

}
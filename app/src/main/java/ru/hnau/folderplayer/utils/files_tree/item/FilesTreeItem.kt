package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter


abstract class FilesTreeItem(
        val context: Context,
        val icon: DrawableGetter
) : PlayItemRelationChecker, Comparable<FilesTreeItem> {

    abstract fun getParent(): OpenableFilesTreeItem?

    abstract fun getTitle(): StringGetter

    override fun compareTo(other: FilesTreeItem): Int {

        val thisPriority = getFilesTreeItemPriority(this)
        val otherPriority = getFilesTreeItemPriority(other)

        if (otherPriority != thisPriority) {
            return otherPriority - thisPriority
        }

        val thisTitle = this.getTitle().get(context)
        val otherTitle = other.getTitle().get(context)

        return thisTitle.compareTo(otherTitle)
    }

    private fun getFilesTreeItemPriority(item: FilesTreeItem): Int {
        return when (item) {
            is FsFileFilesTreeItem -> 1
            is BookmarkFilesTreeItem -> 3
            is FsRootFilesTreeItem -> 4
            is StorageFilesTreeItem -> 5
            is BookmarksFilesTreeItem -> 6
            is RootFilesTreeItem -> 7
            is FsDirFilesTreeItem -> 2
            else -> 0
        }
    }

    companion object {

        fun isEquals(item1: FilesTreeItem?, item2: FilesTreeItem?): Boolean {
            if (item1 == null || item2 == null || item1.javaClass != item2.javaClass) {
                return false
            }
            if (item1 is RootFilesTreeItem || item1 is FsRootFilesTreeItem || item1 is BookmarksFilesTreeItem) {
                return true
            }
            if (item1 is FsFilesTreeItem && item2 is FsFilesTreeItem) {
                return item1.path == item2.path
            }
            return false
        }

    }

}
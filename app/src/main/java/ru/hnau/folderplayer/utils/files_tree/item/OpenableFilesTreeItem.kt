package ru.hnau.folderplayer.utils.files_tree.item


interface OpenableFilesTreeItem {

    fun getSubitems(): List<FilesTreeItem>

}
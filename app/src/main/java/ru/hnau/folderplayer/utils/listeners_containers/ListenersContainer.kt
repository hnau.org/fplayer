package ru.hnau.folderplayer.utils.listeners_containers

import java.util.concurrent.CopyOnWriteArraySet

/**
 * Добавление, удаление и вызов слушателей
 */
class ListenersContainer {

    private val listeners = CopyOnWriteArraySet<() -> Unit>()

    fun add(listener: () -> Unit) = listeners.add(listener)

    fun remove(listener: () -> Unit) = listeners.remove(listener)

    fun call() = listeners.forEach { it.invoke() }

    fun count() = listeners.size

    fun empty() = count() <= 0

    fun clear() = listeners.clear()

}
package ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions

import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.DialogManager


data class DialogAction(
        val title: StringGetter,
        val action: () -> Unit,
        private val closeOnClick: Boolean = true
) {

    fun execute() {
        action.invoke()
        if (closeOnClick) {
            DialogManager.close()
        }
    }

}

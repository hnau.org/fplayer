package ru.hnau.folderplayer.utils.managers.audio

import java.io.File


data class PlayItem(
        val path: String
) {

    private val file: File by lazy { File(path) }

    val filename: String
        get() = file.name
}
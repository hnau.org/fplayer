package ru.hnau.folderplayer.utils.managers.db

import android.database.sqlite.SQLiteDatabase
import android.util.Log
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.managers.Logger


object DbUpdater {

    private val TAG = DbUpdater.javaClass.simpleName

    private val requiredTables = arrayListOf(
            Pair("EQUALIZER_SCHEME", "CREATE TABLE EQUALIZER_SCHEME (ID INTEGER PRIMARY KEY, EDITABLE INT, NAME TEXT, BANDS_VALUES TEXT)"),
            Pair("BOOKMARK", "CREATE TABLE BOOKMARK (ID INTEGER PRIMARY KEY, PATH TEXT)")
    )

    val tablesNames = requiredTables.map { it.first }

    private val updates: HashMap<Int, Array<String>> = hashMapOf()


    fun applyUpdates(db: SQLiteDatabase, toVersion: Int) = Utils.tryOrElse({
        checkAndCreateTables(db)
        val fromVersion = db.version
        if (fromVersion == toVersion) {
            Log.d(TAG, "Db version $fromVersion is actual")
        } else {
            Log.d(TAG, "Need update db from version $fromVersion to version $toVersion")
            ((fromVersion + 1)..toVersion).forEach {
                applyUpdate(db, it)
                db.version = it
            }
        }
    }, {
        throw RuntimeException("Error while update db to version $toVersion", it)
    })

    private fun checkAndCreateTables(db: SQLiteDatabase) = requiredTables.forEach { (name, sql) ->
        val cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '$name'", null)
        val exists = cursor.use { it.count > 0 }
        if (!exists) {
            Logger.d(TAG, "No table $name found, need create")
            db.execSQL(sql)
        }
    }

    private fun applyUpdate(db: SQLiteDatabase, version: Int) = updates[version]?.let { sqls ->
        Log.d(TAG, "Updating db to version $version")
        sqls.forEach { sql -> db.execSQL(sql) }
    }

}
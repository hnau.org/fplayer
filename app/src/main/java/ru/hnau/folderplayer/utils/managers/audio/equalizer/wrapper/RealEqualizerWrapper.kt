package ru.hnau.folderplayer.utils.managers.audio.equalizer.wrapper

import android.media.audiofx.Equalizer
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.managers.audio.equalizer.SchemasContainer


class RealEqualizerWrapper(
        private val equalizer: Equalizer,
        bandsCount: Short,
        private val bandsFreq: List<Int>,
        private val rangeMin: Short,
        private val rangeMax: Short
) : EqualizerWrapper(bandsCount) {

    override fun getBandFreq(bandNum: Int) = bandsFreq.getOrNull(bandNum) ?: 0

    override fun applyScheme(schemeId: Long, schemasContainer: SchemasContainer, onlyThisBand: Int?) {
        if (onlyThisBand != null) {
            applySchemeBand(schemeId, schemasContainer, onlyThisBand)
            return
        }

        (0 until EqualizerManager.bandsCount).forEach {
            applySchemeBand(schemeId, schemasContainer, it)
        }
    }

    private fun applySchemeBand(schemeId: Long, schemasContainer: SchemasContainer, band: Int) {
        val value = schemasContainer.getSchemeValue(schemeId, band)
        applyBand(band, value)
    }

    private fun applyBand(band: Int, value: Float) {
        val normalizedValue = (value + 1) / 2
        val shortValue = (rangeMin + (rangeMax - rangeMin) * normalizedValue).toShort()
        Utils.tryCatch({
            equalizer.setBandLevel(band.toShort(), shortValue)
        }, FirebaseManager::sendThrowable)
    }


}
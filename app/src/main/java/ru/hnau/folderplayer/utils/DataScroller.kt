package ru.hnau.folderplayer.utils

import android.view.animation.Interpolator
import ru.hnau.folderplayer.utils.ui.animations.Animator


class DataScroller<T>(
        private val iterator: (from: T?, to: T?, percentage: Float) -> Unit,
        private val scrollTime: Long = 200,
        private val scrollInterpolator: Interpolator
) {

    private var oldData: T? = null

    var currentData: T? = null
        private set

    private var nextData: T? = null
    private var animateToNext: Boolean = true

    var animating = false
        private set

    fun setNewData(data: T?, animate: Boolean = true) = synchronized(this) {
        animateToNext = animate
        nextData = data
        startAnimationIfNeed()
    }

    private fun startAnimationIfNeed(): Unit = synchronized(this) {
        if (animating || nextData == currentData) {
            return
        }
        oldData = currentData
        currentData = nextData

        if (!animateToNext) {
            iterator.invoke(oldData, currentData, 1f)
            return
        }

        animating = true

        Animator.doAnimation(
                duration = scrollTime,
                interpolator = scrollInterpolator,
                iterator = {
                    iterator.invoke(oldData, currentData, it)
                },
                endListener = {
                    synchronized(this) {
                        animating = false
                        startAnimationIfNeed()
                    }
                }
        )
    }


}
package ru.hnau.folderplayer.utils.managers.audio.equalizer

import ru.hnau.folderplayer.utils.setSize

class EqualizerScheme(
        val id: Long,
        val editable: Boolean,
        name: String,
        private val values: MutableList<Float>
) {

    constructor(
            id: Long,
            editable: Boolean,
            name: String,
            valuesString: String
    ) : this(
            id = id,
            editable = editable,
            name = name,
            values = stringToValues(valuesString)
    )

    var changed = false
        private set

    var onValueChangedListener: ((band: Int) -> Unit)? = null
    var onNameChangedListener: ((name: String) -> Unit)? = null

    var name: String = name
        set(value) {
            if (field != value) {
                field = value
                changed = true
                onNameChangedListener?.invoke(value)
            }
        }

    val bandsCount = values.size

    fun onSaved() {
        changed = false
    }

    operator fun get(band: Int) = values.getOrNull(band) ?: 0f

    operator fun set(band: Int, value: Float) {
        if (band < 0 || band > values.size) {
            return
        }
        values[band] = normalizeValue(value)
        changed = true
        onValueChangedListener?.invoke(band)
    }

    fun getValuesString() = values.joinToString(separator = VALUES_IN_STRING_SEPARATOR)

    private fun normalizeValue(value: Float) = if (value < -1) -1f else if (value > 1) 1f else value

    fun copy(id: Long) = EqualizerScheme(
            id = id,
            values = ArrayList(values),
            editable = true,
            name = name
    )

    companion object {

        private val VALUES_IN_STRING_SEPARATOR = "|"

        private fun stringToValues(string: String): MutableList<Float> {
            val result = string.split(VALUES_IN_STRING_SEPARATOR).map { it.toFloatOrNull() ?: 0f }.toMutableList()
            result.setSize(EqualizerManager.bandsCount.toInt(), 0f)
            return result
        }

    }

}
package ru.hnau.folderplayer.utils.managers.ux.dialog_manager

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import ru.hnau.folderplayer.activity.view.button.TextButton
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions.DialogAction
import ru.hnau.folderplayer.utils.managers.ux.dialog_manager.actions.DialogActionsList
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class DialogBuilder(val context: Context) {

    var cancellable = true
        private set

    var verticalButtons = false
        private set

    var titleView: View? = null
        private set

    var contentView: View? = null
        private set

    private val buttonsInner = ArrayList<DialogButton>()

    var onCancelledCallback: (() -> Unit)? = null
        private set

    val buttons: List<DialogButton>
        get() = buttonsInner

    fun notCancellable() {
        cancellable = false
    }

    fun verticalButtons() {
        verticalButtons = true
    }

    fun title(view: View) {
        titleView = view
    }

    fun title(text: StringGetter, size: SizeGetter = SizeGetter(24), color: Int = ColorGetter.FG, gravity: Int = Gravity.CENTER_VERTICAL or Gravity.LEFT, maxLines: Int? = null, minLines: Int? = null, ellipsize: TextUtils.TruncateAt = TextUtils.TruncateAt.END) =
            title(Label(context, text, size, color, gravity, maxLines, minLines, ellipsize).apply {
                setBackgroundColor(ColorGetter.BG)
                setPadding(DialogManager.PADDING_H, DialogManager.PADDING_V, DialogManager.PADDING_H, DialogManager.PADDING_V)
            })

    fun titleForActions(text: StringGetter, size: SizeGetter = SizeGetter(17), color: Int = ColorGetter.FG_TRANSPARENT_50, gravity: Int = Gravity.CENTER_VERTICAL or Gravity.LEFT, maxLines: Int? = 1, minLines: Int? = 1, ellipsize: TextUtils.TruncateAt = TextUtils.TruncateAt.END) =
            title(text, size, color, gravity, maxLines, minLines, ellipsize)

    fun content(view: View) {
        contentView = view
    }

    fun content(text: StringGetter, size: SizeGetter = SizeGetter(17), color: Int = ColorGetter.FG_TRANSPARENT_50, gravity: Int = Gravity.CENTER_VERTICAL or Gravity.LEFT, maxLines: Int? = null, minLines: Int? = null, ellipsize: TextUtils.TruncateAt = TextUtils.TruncateAt.END) =
            content(Label(context, text, size, color, gravity, maxLines, minLines, ellipsize).apply {
                setBackgroundColor(ColorGetter.BG)
                setPadding(DialogManager.PADDING_H, 0, DialogManager.PADDING_H, DialogManager.PADDING_V * 2)
            })

    fun content(actions: List<DialogAction>) = content(DialogActionsList(context, actions))

    fun addButton(text: StringGetter, onClickListener: () -> Unit, closeDialogOnCLick: Boolean = true, size: TextButton.Size = TextButton.Size.SMALL, color: TextButton.Color = TextButton.Color.NORMAL) =
            buttonsInner.add(DialogButton(context, text, onClickListener, closeDialogOnCLick, size, color).apply {
                val paddingH = dpToPxInt(16)
                val paddingV = dpToPxInt(16)
                setPadding(paddingH, paddingV, paddingH, paddingV)
            })

    fun onCancelledCallback(onCancelledCallback: () -> Unit) {
        this.onCancelledCallback = onCancelledCallback
    }

}
package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.managers.audio.PlayItem
import java.io.File


open class FsDirFilesTreeItem(
        context: Context,
        path: String,
        icon: DrawableGetter = DrawableGetter(R.drawable.ic_files_tree_item_dir),
        file: File = File(path)
) : FsFilesTreeItem(
        context,
        path,
        icon,
        file
), OpenableFilesTreeItem {

    override fun getSubitems() = FsFilesTreeItem.getFileSubitems(context, file)

}
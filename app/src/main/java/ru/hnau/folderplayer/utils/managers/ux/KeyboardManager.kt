package ru.hnau.folderplayer.utils.managers.ux

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import ru.hnau.folderplayer.utils.managers.AppActivityManager


object KeyboardManager {

    private fun getInputMethodManager(context: Context) = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    fun showAndRequestFocus(view: View?) {
        if (view == null) {
            return
        }
        if (view.requestFocus()) {
            getInputMethodManager(view.context).showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun hide() {
        val rootView = AppActivityManager.activity?.rootView ?: return
        getInputMethodManager(rootView.context).hideSoftInputFromWindow(rootView.windowToken, 0)
    }
}
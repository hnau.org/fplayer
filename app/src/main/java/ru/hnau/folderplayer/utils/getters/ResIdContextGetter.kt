package ru.hnau.folderplayer.utils.getters

import android.content.Context


abstract class ResIdContextGetter<T>(private val resId: Int?, data: T? = null): ContextGetter<T>(data) {

    constructor(data: T): this(null, data)

    protected override fun generate(context: Context) = generate(context, resId)

    protected abstract fun generate(context: Context, resId: Int?): T

}
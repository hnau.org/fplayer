package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import java.io.File


class FsRootFilesTreeItem(
        context: Context
) : FsDirFilesTreeItem(
        context,
        "/",
        DrawableGetter(R.drawable.ic_files_tree_item_fs_root)
) {

    override fun getTitle() = StringGetter(R.string.fs_item_title_fs_root)

}
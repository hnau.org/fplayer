package ru.hnau.folderplayer.utils.ui.animations

import android.view.animation.Interpolator
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import ru.hnau.folderplayer.utils.ui.animations.Animator

class TwoStateAnimator(initWithPositive: Boolean = false) {
    private val DEFAULT_ANIMATION_TIME_POSITIVE = 200L
    private val DEFAULT_ANIMATION_TIME_NEGATIVE = 200L

    var iterator: ((Float) -> Unit)? = null
    var interpolatorPositive: Interpolator = FastOutSlowInInterpolator()
    var interpolatorNegative: Interpolator = FastOutSlowInInterpolator()
    var timePositive = DEFAULT_ANIMATION_TIME_POSITIVE
    var timeNegative = DEFAULT_ANIMATION_TIME_NEGATIVE

    var time: Long
        set(value) {
            timePositive = value
            timeNegative = value
        }
        get() = timePositive

    var interpolator: Interpolator
        set(value) {
            interpolatorPositive = value
            interpolatorNegative = value
        }
        get() = interpolatorPositive

    private var targetIsImmediately: Boolean = false
    private var targetPositionIsPositive: Boolean = false
    private var currentPositionIsPositive: Boolean = false
    var animating: Boolean = false
        private set

    val targetIsPositive: Boolean
        get() = targetPositionIsPositive

    var currentValue = 0f
        private set

    init {
        targetPositionIsPositive = initWithPositive
        currentPositionIsPositive = initWithPositive
    }

    fun animateToPositive() {
        addNewTarget(true, false)
    }

    fun animateToNegative() {
        addNewTarget(false, false)
    }

    fun switchToPositive() {
        addNewTarget(true, true)
    }

    fun switchToNegative() {
        addNewTarget(false, true)
    }

    fun animateOrSwitch(animate: Boolean) = addNewTarget(!targetPositionIsPositive, !animate)

    fun animate() = animateOrSwitch(true)

    fun switch() = animateOrSwitch(false)

    fun animateTo(positive: Boolean) = addNewTarget(positive, false)

    fun switchTo(positive: Boolean) = addNewTarget(positive, true)

    fun addNewTarget(newTargetIsPositive: Boolean, newTargetIsImmediately: Boolean = false) {
        synchronized(targetPositionIsPositive, {
            if (targetPositionIsPositive == newTargetIsPositive) {
                return
            }
            targetPositionIsPositive = newTargetIsPositive
            targetIsImmediately = newTargetIsImmediately
        })
        startAnimationIfNeed()
    }

    private fun startAnimationIfNeed() {
        synchronized(animating, {
            if (animating) {
                return
            }
            animating = true
        })

        synchronized(targetPositionIsPositive, {
            if (targetPositionIsPositive == currentPositionIsPositive) {
                animating = false
                return
            }
            currentPositionIsPositive = targetPositionIsPositive
        })

        if (targetIsImmediately) {
            callIterator(if (currentPositionIsPositive) 1f else 0f)
            animating = false
            return
        }

        if (targetPositionIsPositive) {
            startPositiveAnimation()
        } else {
            startNegativeAnimation()
        }
    }

    private fun startPositiveAnimation() {
        Animator.doAnimation(timePositive, this::callIterator, interpolatorPositive, {
            animating = false
            startAnimationIfNeed()
        })
    }

    private fun startNegativeAnimation() {
        Animator.doAnimation(timeNegative, { callIterator(1 - it) }, interpolatorNegative, {
            animating = false
            startAnimationIfNeed()
        })
    }

    private fun callIterator(value: Float) {
        currentValue = value
        iterator?.invoke(value)
    }


}
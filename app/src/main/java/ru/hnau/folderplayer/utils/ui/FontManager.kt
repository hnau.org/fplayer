package ru.hnau.folderplayer.utils.ui
import android.graphics.Paint
import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import ru.hnau.folderplayer.utils.managers.ContextContainer


object FontManager {

    private val FONT_FILENAME = "PTSans.ttf"

    private val typeface = Typeface.createFromAsset(ContextContainer.context.assets, FONT_FILENAME)!!

    fun getPaint() = Paint(Paint.ANTI_ALIAS_FLAG).apply { typeface = this@FontManager.typeface }

    fun fontView(view: View) {
        if (view !is TextView) {
            return
        }

        val viewTypeface = if (view.typeface == null) {
            typeface
        } else {
            Typeface.create(typeface, view.typeface.style)!!
        }

        view.typeface = viewTypeface
    }

}
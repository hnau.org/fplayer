package ru.hnau.folderplayer.utils.managers

import android.annotation.SuppressLint
import android.content.Context

@SuppressLint("StaticFieldLeak")
object ContextContainer {

    lateinit var context: Context
        private set

    fun onContextChanged(context: Context) {
        this.context = context
    }

}
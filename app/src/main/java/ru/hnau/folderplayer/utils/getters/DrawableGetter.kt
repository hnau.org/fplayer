package ru.hnau.folderplayer.utils.getters

import android.content.Context
import android.graphics.drawable.Drawable
import ru.hnau.folderplayer.utils.ui.drawable.EmptyDrawable


class DrawableGetter(resId: Int?, data: Drawable? = null) : ResIdContextGetter<Drawable>(resId, data) {

    constructor(data: Drawable): this(null, data)

    override fun generate(context: Context, resId: Int?) = resId?.let { context.resources.getDrawable(resId) } ?: EmptyDrawable()

}
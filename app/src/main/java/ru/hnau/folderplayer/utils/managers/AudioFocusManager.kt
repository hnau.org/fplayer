package ru.hnau.folderplayer.utils.managers

import android.content.Context
import android.media.AudioManager
import ru.hnau.folderplayer.utils.listeners_containers.ListenersDataContainer
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager


object AudioFocusManager : AudioManager.OnAudioFocusChangeListener {

    private val audioManager = ContextContainer.context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

    private val onStateChangedListenersContainer = ListenersDataContainer<Boolean>(
            onFirstAdded = this::enable,
            onLastRemoved = this::disable
    )

    fun addOnStateChangedListener(listener: (Boolean) -> Unit) =
            onStateChangedListenersContainer.add(listener)

    fun removeOnStateChangedListener(listener: (Boolean) -> Unit) =
            onStateChangedListenersContainer.remove(listener)

    private fun enable() {
        audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
    }

    private fun disable() {
        audioManager.abandonAudioFocus(this)
    }

    override fun onAudioFocusChange(focusChange: Int) {
        when (focusChange) {
            AudioManager.AUDIOFOCUS_LOSS,
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                onStateChangedListenersContainer.call(false)
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                PlayerManager.quiet = true
            }
            AudioManager.AUDIOFOCUS_GAIN,
            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT,
            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE -> {
                onStateChangedListenersContainer.call(true)
            }
            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK -> {
                PlayerManager.quiet = false
            }
        }
    }

}
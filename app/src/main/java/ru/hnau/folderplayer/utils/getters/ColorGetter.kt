package ru.hnau.folderplayer.utils.getters

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.managers.ContextContainer


object ColorGetter {

    private val DEFAULT_COLOR = Color.TRANSPARENT

    var TRANSPARENT: Int = DEFAULT_COLOR
        private set

    var PRIMARY: Int = DEFAULT_COLOR
        private set

    var FG: Int = DEFAULT_COLOR
        private set

    var FG_TRANSPARENT_50: Int = DEFAULT_COLOR
        private set

    var BG: Int = DEFAULT_COLOR
        private set

    var BG_DARK: Int = DEFAULT_COLOR
        private set

    var SELECT: Int = DEFAULT_COLOR
        private set

    var SELECT_DARK: Int = DEFAULT_COLOR
        private set

    init {
        val context = ContextContainer.context
        TRANSPARENT = ContextCompat.getColor(context, R.color.transparent)
        PRIMARY = ContextCompat.getColor(context, R.color.primary)
        FG = ContextCompat.getColor(context, R.color.fg)
        FG_TRANSPARENT_50 = ContextCompat.getColor(context, R.color.fg_50)
        BG = ContextCompat.getColor(context, R.color.bg)
        BG_DARK = ContextCompat.getColor(context, R.color.bg_dark)
        SELECT = ContextCompat.getColor(context, R.color.select)
        SELECT_DARK = ContextCompat.getColor(context, R.color.select_dark)
    }

}
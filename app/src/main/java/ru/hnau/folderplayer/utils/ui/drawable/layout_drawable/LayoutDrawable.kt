package ru.hnau.folderplayer.utils.ui.drawable.layout_drawable

import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.View
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.ui.drawable.EmptyDrawable

import ru.hnau.folderplayer.utils.ui.drawable.TranslateBeforeDrawDrawable
import ru.hnau.folderplayer.utils.ui.preferredHeight
import ru.hnau.folderplayer.utils.ui.preferredWidth


class LayoutDrawable(
        private val context: Context,
        initialIcon: DrawableGetter? = null,
        private val layoutType: LayoutType = LayoutType.PROPORTIONAL_IN,
        private val gravity: Int = Gravity.CENTER,
        private val padding: Rect = Rect(),
        private val scale: Float = 1f
) : TranslateBeforeDrawDrawable(), Drawable.Callback {

    enum class LayoutType {
        MATCH_PARENT,
        WRAP_CONTENT,
        PROPORTIONAL_IN,
        PROPORTIONAL_OUT,
    }

    var icon = initialIcon
        set(value) {
            if (field != value) {
                val old = field
                field = value
                if (old != null) {
                    old.get(context).callback = null
                }
                if (value != null) {
                    value.get(context).callback = this@LayoutDrawable
                }
                invalidateSelf()
            }
        }

    companion object {

        private val EMPTY_DRAWABLE = EmptyDrawable()

    }

    private val content: Drawable
        get() = icon?.get(context) ?: EMPTY_DRAWABLE

    private val contentWidth: Float
        get() = content.preferredWidth() * scale

    private val contentHeight: Float
        get() = content.preferredHeight() * scale

    private val aspectRatio = if (contentWidth <= 0 || contentHeight <= 0) null else contentWidth / contentHeight

    override fun getIntrinsicWidth() = contentWidth.toInt()
    override fun getIntrinsicHeight() = contentHeight.toInt()

    override fun draw(canvas: Canvas, width: Int, height: Int) {
        val left = padding.left
        val top = padding.top
        val right = width - padding.right
        val bottom = height - padding.bottom
        val window = Rect(left, top, right, bottom)
        canvas.save()
        canvas.clipRect(window)

        when (layoutType) {
            LayoutType.MATCH_PARENT -> drawMatchParent(canvas, window)
            LayoutType.WRAP_CONTENT -> drawWrapContent(canvas, window)
            LayoutType.PROPORTIONAL_IN -> drawProportionalIn(canvas, window)
            LayoutType.PROPORTIONAL_OUT -> drawProportionalOut(canvas, window)
        }

        canvas.restore()
    }

    private fun drawMatchParent(canvas: Canvas, window: Rect) {
        content.bounds = window
        content.draw(canvas)
    }

    private fun drawWrapContent(canvas: Canvas, window: Rect) {
        if (contentWidth <= 0 || contentHeight <= 0) {
            return
        }
        Gravity.apply(gravity, contentWidth.toInt(), contentHeight.toInt(), window, content.bounds)
        content.draw(canvas)
    }

    private fun drawProportionalIn(canvas: Canvas, window: Rect) {
        val drawableAspectRatio = aspectRatio ?: return
        val windowWidth = window.width()
        val windowHeight = window.height()
        if (windowWidth <= 0 || windowHeight <= 0) {
            return
        }
        val windowAspectRatio = windowWidth.toFloat() / windowHeight.toFloat()
        val drawableSize = if (drawableAspectRatio > windowAspectRatio) {
            Point(windowWidth, (windowWidth / drawableAspectRatio).toInt())
        } else {
            Point((windowHeight * drawableAspectRatio).toInt(), windowHeight)
        }
        Gravity.apply(gravity, drawableSize.x, drawableSize.y, window, content.bounds)
        content.draw(canvas)
    }

    private fun drawProportionalOut(canvas: Canvas, window: Rect) {
        val drawableAspectRatio = aspectRatio ?: return
        val windowWidth = window.width()
        val windowHeight = window.height()
        if (windowWidth <= 0 || windowHeight <= 0) {
            return
        }
        val windowAspectRatio = windowWidth.toFloat() / windowHeight.toFloat()
        val drawableSize = if (drawableAspectRatio < windowAspectRatio) {
            Point(windowWidth, (windowWidth / drawableAspectRatio).toInt())
        } else {
            Point((windowHeight * drawableAspectRatio).toInt(), windowHeight)
        }
        val rect = Rect()
        Gravity.apply(gravity, windowWidth, windowHeight, Rect(0, 0, drawableSize.x, drawableSize.y), rect)
        val left = -rect.left
        val top = -rect.top
        content.bounds = Rect(left, top, left + drawableSize.x, top + drawableSize.y)
        content.draw(canvas)

    }

    override fun unscheduleDrawable(p0: Drawable, p1: Runnable) = callback?.unscheduleDrawable(p0, p1) ?: Unit

    override fun invalidateDrawable(p0: Drawable) = callback?.invalidateDrawable(p0) ?: Unit

    override fun scheduleDrawable(p0: Drawable, p1: Runnable, p2: Long) = callback?.scheduleDrawable(p0, p1, p2) ?: Unit
}
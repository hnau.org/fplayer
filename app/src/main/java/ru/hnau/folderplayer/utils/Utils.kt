package ru.hnau.folderplayer.utils

import android.os.Handler
import android.os.Looper
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.Logger
import java.util.*


object Utils {

    private val TAG = Utils::class.java.simpleName

    private val RANDOM = Random(System.currentTimeMillis())

    fun tryCatch(action: () -> Unit) = tryCatch(action, null)

    fun tryCatch(action: () -> Unit, ifException: ((ex: Throwable) -> Unit)? = null) {
        try {
            action.invoke()
        } catch (ex: Throwable) {
            Logger.w(TAG, "Error while execute action", ex)
            FirebaseManager.sendThrowable(ex)
            ifException?.invoke(ex)
        }
    }

    fun runOnUiThread(action: Runnable) = Handler(Looper.getMainLooper()).post(action)

    fun <T> tryOrElse(action: () -> T, elseAction: (ex: Throwable) -> T) = try {
        action.invoke()
    } catch (ex: Throwable) {
        Logger.w(TAG, "Error while execute action", ex)
        FirebaseManager.sendThrowable(ex)
        elseAction.invoke(ex)
    }

    fun <T> tryOrNull(action: () -> T) = tryOrElse(action, { null })

    fun generateId() = RANDOM.nextInt() and 0x0000ffff

    fun timeToStr(seconds: Int, showHours: Boolean? = null): String {
        var sec = seconds
        var min = sec / 60
        sec -= min * 60
        val hour = min / 60
        min -= hour * 60
        if (showHours == true || showHours == null && hour > 0) {
            return "${valueToStr(hour)}:${valueToStr(min)}:${valueToStr(sec)}"
        }
        return "${valueToStr(min)}:${valueToStr(sec)}"
    }

    private fun valueToStr(value: Int) = if (value < 10) "0$value" else value.toString()

}

fun <T> Iterable<T>.findPos(predicate: (T) -> Boolean): Int? {
    forEachIndexed { index, item ->
        if (predicate.invoke(item)) {
            return index
        }
    }
    return null
}

fun <T> List<T>.circledGet(pos: Int): T? {
    val size = this.size
    val normalizedPos = if (pos < 0) pos + size else pos % size
    return getOrNull(normalizedPos)
}

fun <T> List<T>.getOrFirst(pos: Int): T = getOrNull(pos) ?: get(0)

fun Int.toBoolean() = this == 1

fun Boolean.toInt() = if (this) 1 else 0

fun <T> MutableList<T>.setSize(newSize: Int, emptyElement: T) {
    val oldSize = size
    val countDelta = newSize - oldSize

    when {
        countDelta > 0 -> (0 until countDelta).forEach { add(emptyElement) }
        countDelta < 0 -> {
            dropLast(-countDelta)
        }
    }
}
package ru.hnau.folderplayer.utils.files_tree.item

import android.content.Context
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.utils.managers.bookmarks.BookmarksManager
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.audio.PlayItem

class BookmarksFilesTreeItem(
        context: Context
) : FilesTreeItem(
        context,
        DrawableGetter(R.drawable.ic_files_tree_item_bookmark)
), OpenableFilesTreeItem {

    override fun getSubitems() = BookmarksManager.getBookmarks(context)

    override fun getParent() = RootFilesTreeItem(context)

    override fun getTitle() = StringGetter(R.string.fs_item_title_bookmarks)

    override fun getPlayItemRelation(playItem: PlayItem): PlayItemRelationChecker.Type {
        val bookmarks = BookmarksManager.getBookmarks(context)
        bookmarks.forEach {
            if (it.getPlayItemRelation(playItem) != PlayItemRelationChecker.Type.NONE) {
                return PlayItemRelationChecker.Type.PARENT
            }
        }
        return PlayItemRelationChecker.Type.NONE
    }

}
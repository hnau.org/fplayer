package ru.hnau.folderplayer.utils.ui.animations

import android.os.Build

/**
 * Управление уровнем разрешенных анимаций
 */
object AnimationManager {

    val ANIMATION_STEP_TIME = 10L

    enum class Type(val value: Int) { NONE(0), SIMPLE(1), FULL(2) }

    fun getType() = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) Type.NONE else Type.FULL

    fun canUse(type: Type) = getType().value >= type.value

    fun canUseSimple() = canUse(Type.SIMPLE)

    fun canUseFull() = canUse(Type.FULL)

}
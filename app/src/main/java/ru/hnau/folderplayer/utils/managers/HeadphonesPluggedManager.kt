package ru.hnau.folderplayer.utils.managers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import ru.hnau.folderplayer.utils.listeners_containers.ListenersDataContainer


object HeadphonesPluggedManager : BroadcastReceiver() {

    private val intentFilter = IntentFilter().apply {
        addAction("android.intent.action.HEADSET_PLUG")
    }

    private val onPluggedStateChangedListenersContainer = ListenersDataContainer<Boolean>(
            onFirstAdded = this::enable,
            onLastRemoved = this::disable
    )

    private var active = false

    fun addOnPluggedStateChangedListener(listener: (Boolean) -> Unit) =
            onPluggedStateChangedListenersContainer.add(listener)

    fun removeOnPluggedStateChangedListener(listener: (Boolean) -> Unit) =
            onPluggedStateChangedListenersContainer.remove(listener)

    private fun enable() {
        ContextContainer.context.registerReceiver(this, intentFilter)
        Handler().postDelayed({
            active = true
        }, 100)
    }

    private fun disable() {
        active = false
        ContextContainer.context.unregisterReceiver(this)
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (!active) {
            return
        }
        val state = intent.getIntExtra("state", -1)
        when (state) {
            0 -> onPluggedStateChangedListenersContainer.call(false)
            1 -> onPluggedStateChangedListenersContainer.call(true)
        }
    }


}
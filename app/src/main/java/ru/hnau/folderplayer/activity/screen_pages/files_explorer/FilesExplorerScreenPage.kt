package ru.hnau.folderplayer.activity.screen_pages.files_explorer

import android.content.Context
import ru.hnau.folderplayer.activity.screen_pages.ScreenPage
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager


class FilesExplorerScreenPage : ScreenPage<FilesExplorerViewWithPermissionsChecker>() {

    override fun createView(context: Context) = FilesExplorerViewWithPermissionsChecker(context)

    override fun handleGoBack(): Boolean {
        if (!PreferencesManager.goToUpDirOnBackClicked) {
            return false
        }
        return view?.handleGoBack() ?: super.handleGoBack()
    }
}
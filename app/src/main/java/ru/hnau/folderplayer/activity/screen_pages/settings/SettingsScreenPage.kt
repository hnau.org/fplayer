package ru.hnau.folderplayer.activity.screen_pages.settings

import android.content.Context
import ru.hnau.folderplayer.activity.screen_pages.ScreenPage
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager
import ru.hnau.folderplayer.utils.ui.ScaleManager


class SettingsScreenPage : ScreenPage<SettingsScreenPageView>() {

    override fun createView(context: Context) = SettingsScreenPageView(context)

    override fun onHide() {
        super.onHide()

        FirebaseManager.sendEvent("On settings updated", getParams())
    }

    private fun getParams() = hashMapOf(
            "goToUpDirOnBackClicked" to PreferencesManager.goToUpDirOnBackClicked.toString(),
            "uxScaleId" to Math.round((ScaleManager.values[PreferencesManager.uxScaleId]
                    ?: -1f) * 100).toString() + "%",
            "pauseOnHeadphonesUnplugged" to PreferencesManager.pauseOnHeadphonesUnplugged.toString(),
            "resumeOnHeadphonesPlugged" to PreferencesManager.resumeOnHeadphonesPlugged.toString(),
            "pauseOnUnfocused" to PreferencesManager.pauseOnUnfocused.toString(),
            "resumeOnFocused" to PreferencesManager.resumeOnFocused.toString(),
            "resumeWhereStopped" to PreferencesManager.resumeWhereStopped.toString(),
            "resumeWhereStoppedInactiveWidget" to PreferencesManager.resumeWhereStoppedInactiveWidget.toString()
    )

}
package ru.hnau.folderplayer.activity.view

import android.content.Context
import android.view.View
import android.widget.LinearLayout


class LinearLayoutSeparator(
        context: Context,
        weight: Float = 1f,
        width: Int = 0,
        height: Int = 0
) : View(
        context
) {

    init {
        layoutParams = LinearLayout.LayoutParams(width, height, weight)
    }

}
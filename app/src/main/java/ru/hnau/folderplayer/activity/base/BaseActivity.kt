package ru.hnau.folderplayer.activity.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.app.ComponentActivity
import ru.hnau.folderplayer.activity.view.bottom_animator.BottomAnimatorView


abstract class BaseActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        initStatusBarColor()
        super.onCreate(savedInstanceState)
    }

    var rootView: BaseActivityContainerView? = null
        private set

    override fun setContentView(view: View) {
        val containerView = BaseActivityContainerView(view)
        this.rootView = containerView
        super.setContentView(containerView)
    }

    private fun initStatusBarColor() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return
        }
        window.statusBarColor = Color.TRANSPARENT
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    }

    fun showBottomView(view: BottomAnimatorView) = rootView?.showBottomView(view)

    fun hideBottomView() = rootView?.hideBottomView()

    final override fun onBackPressed() = goBack()

    fun goBack() {
        if (rootView?.handleGoBack() == true) {
            return
        }

        if (handleGoBack()) {
            return
        }

        finish()
    }

    open fun handleGoBack() = false

}
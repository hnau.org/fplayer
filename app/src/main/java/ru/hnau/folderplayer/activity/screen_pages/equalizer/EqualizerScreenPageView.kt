package ru.hnau.folderplayer.activity.screen_pages.equalizer

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.screen_pages.equalizer.editor.EqualizerSchemeEditor
import ru.hnau.folderplayer.activity.screen_pages.equalizer.list.EqualizerSchemasList
import ru.hnau.folderplayer.activity.view.header.Header
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.ui.UiUtils


class EqualizerScreenPageView(context: Context) : LinearLayout(context) {

    private val header = Header(
            context = context,
            title = StringGetter(R.string.equalizer_screen_page_title)
    ).apply {
        addButton(
                icon = DrawableGetter(R.drawable.ic_equalizer_screen_header_button_add),
                onClick = { EqualizerManager.duplicateScheme(EqualizerManager.currentSchemeId) }
        )
    }

    private val schemeEditor = EqualizerSchemeEditor(context).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, 0, 1f).apply {
            bottomMargin = UiUtils.ELEMENTS_SEPARATION
        }
    }

    private val schemasList = EqualizerSchemasList(context).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, 0, 1.5f)
    }

    init {
        orientation = VERTICAL
        addView(header)
        addView(schemeEditor)
        addView(schemasList)
    }

}
package ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import ru.hnau.folderplayer.activity.screen_pages.settings.item.SettingsItem
import ru.hnau.folderplayer.activity.screen_pages.settings.item.SettingsProperty
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.SettingsItemTitleLabelView
import ru.hnau.folderplayer.activity.view.ClickableLinearLayout
import ru.hnau.folderplayer.activity.view.collapsible_view.CollapsibleList
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.activity.view.list.BaseListAdapter
import ru.hnau.folderplayer.activity.view.views_switcher.ViewsSwitcher
import ru.hnau.folderplayer.activity.view.views_switcher.layout_manager.ViewMeasuredHeightViewsSwitcherLayoutManager
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.listeners_containers.ListenersContainer
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class SettingsItemChoiceView(context: Context) : LinearLayout(context) {

    private val titleView = SettingsItemTitleLabelView(context).apply {
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 2f).apply {
            rightMargin = dpToPxInt(8)
        }
    }

    private val switcherView = ViewsSwitcher(context).apply {
        animationTime = 150
        layoutManager = ViewMeasuredHeightViewsSwitcherLayoutManager()
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.WRAP_CONTENT, 1f)
    }

    private val topContent = ClickableLinearLayout(context).apply {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER
        setBackgroundColor(ColorGetter.BG)

        val paddingH = SettingsItem.PADDING_H
        val paddingV = SettingsItem.PADDING_V
        setPadding(paddingH, paddingV, paddingH, paddingV)

        addView(titleView)
        addView(switcherView)
    }.apply {
        setOnClickListener { listView.switch() }
    }

    private val listAdapter = object : BaseListAdapter<SettingsItemChoiceViewItem?>() {
        override fun bindItem(view: View, item: SettingsItemChoiceViewItem?, type: Int) {
            (view as? SettingsItemChoiceViewItemView)?.item = item
        }

        override fun getItemCount() = values?.size ?: 0

        override fun getItem(position: Int) = values?.get(position)

        override fun generateView(type: Int) = SettingsItemChoiceViewItemView(
                context = context,
                onClickListener = this@SettingsItemChoiceView::onSelectedItemChanged,
                addOnSelectedStateChangedListenerCallback = addOnSelectedStateChangedListenerCallback,
                removeOnSelectedStateChangedListenerCallback = removeOnSelectedStateChangedListenerCallback,
                selectedGetter = selectedItemGetter
        ).apply {
            layoutParams = RecyclerView.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT).apply {
                topMargin = UiUtils.ELEMENTS_SEPARATION
            }
        }

    }

    private val listView = CollapsibleList.create<SettingsItemChoiceViewItem?>(
            context = context,
            maxContentHeight = null
    ).apply {
        adapter = listAdapter
    }

    private val onSelectedItemChangedListenersContainer = ListenersContainer()

    private val addOnSelectedStateChangedListenerCallback: (() -> Unit) -> Unit = { onSelectedItemChangedListenersContainer.add(it) }
    private val removeOnSelectedStateChangedListenerCallback: (() -> Unit) -> Unit = { onSelectedItemChangedListenersContainer.remove(it) }

    private val selectedItemGetter = { selectedItemProperty?.get() }

    var title: StringGetter? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                titleView.text = value.get(context)
            }
        }

    var selectedItemPropertyWithValues: Pair<SettingsItemChoiceProperty, List<SettingsItemChoiceViewItem>>? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                propertyWithListChanged()
            }
        }

    private val selectedItemProperty: SettingsItemChoiceProperty?
        get() = selectedItemPropertyWithValues?.first

    private val values: List<SettingsItemChoiceViewItem>?
        get() = selectedItemPropertyWithValues?.second

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        setBackgroundColor(ColorGetter.BG_DARK)

        addView(topContent)
        addView(listView)
    }

    private fun propertyWithListChanged() {
        listAdapter.notifyDataSetChanged()
        val item = selectedItemProperty?.get() ?: return
        updateSwitcher(item, false)
    }

    private fun onSelectedItemChanged(item: SettingsItemChoiceViewItem) {
        listView.collapse()
        selectedItemProperty?.set(item)
        onSelectedItemChangedListenersContainer.call()
        updateSwitcher(item, true)
    }

    private fun updateSwitcher(item: SettingsItemChoiceViewItem, animate: Boolean) {
        val view = getItemViewForSwitcher(item)
        switcherView.showView(view, ViewsSwitcher.Side.BOTTOM, animate)
    }

    private fun getItemViewForSwitcher(item: SettingsItemChoiceViewItem) = Label(
            context = context,
            gravity = Gravity.RIGHT or Gravity.CENTER_VERTICAL,
            text = StringGetter(item.getDescription()),
            size = SizeGetter(17),
            color = ColorGetter.PRIMARY,
            maxLines = 1,
            minLines = 1
    )


}
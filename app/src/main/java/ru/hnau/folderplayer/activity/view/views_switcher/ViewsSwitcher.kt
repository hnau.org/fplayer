package ru.hnau.folderplayer.activity.view.views_switcher

import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import ru.hnau.folderplayer.activity.view.views_switcher.layout_manager.SimpleViewsSwitcherLayoutManager
import ru.hnau.folderplayer.activity.view.views_switcher.layout_manager.ViewsSwitcherLayoutManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.Animator

/**
 * Показ и сокрытие элементов с анимацией
 */
open class ViewsSwitcher(context: Context) : ViewGroup(context) {

    companion object {
        private val INTERPOLATOR = FastOutSlowInInterpolator()
        private val DEFAULT_ANIMATION_TIME = 300L
    }

    enum class Side { LEFT, TOP, RIGHT, BOTTOM }

    protected data class SwitchingData(
            val fromSide: Side,
            val view: View?,
            val animate: Boolean,
            var viewRect: Rect?
    )

    var scrollFactor: Float = 1f

    var layoutManager: ViewsSwitcherLayoutManager = SimpleViewsSwitcherLayoutManager()

    var animationTime: Long = DEFAULT_ANIMATION_TIME

    private var nextSwitchingData: SwitchingData? = null
    protected var currentSwitchingData: SwitchingData? = null
    private var lastSwitchingData: SwitchingData? = null

    val currentView
        get() = currentSwitchingData?.view

    val lastView
        get() = lastSwitchingData?.view

    private var updating = false
        set(value) {
            field = value
            requestLayout()
        }

    protected var offset: Float = 1f
        set(value) {
            field = value
            onSwitchingOffsetChanged(value)
        }

    var animating = false
        private set

    var initialized = false
        private set

    protected open fun onSwitchingOffsetChanged(offset: Float) {
        requestLayout()
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (updating) {
            return
        }
        val currentSwitchingData = currentSwitchingData ?: return
        val fromSide = currentSwitchingData.fromSide
        val horizontal = fromSide == Side.LEFT || fromSide == Side.RIGHT
        val scrollSize = if (horizontal) width else height
        val offsetRaw = (this.offset * scrollSize * scrollFactor).toInt()
        val offset = if (fromSide == Side.BOTTOM || fromSide == Side.RIGHT) -offsetRaw else offsetRaw
        val offsetX = if (horizontal) offset else 0
        val offsetY = if (horizontal) 0 else offset
        val currentStartX = if (!horizontal) 0 else ((if (fromSide == Side.LEFT) -width else width) * scrollFactor).toInt()
        val currentStartY = if (horizontal) 0 else ((if (fromSide == Side.TOP) -height else height) * scrollFactor).toInt()

        val currentAlpha = if (scrollFactor >= 1) 1f else this.offset
        layoutView(currentSwitchingData, currentStartX, currentStartY, offsetX, offsetY, currentAlpha)
        val lastAlpha = if (scrollFactor >= 1) 1f else (1 - this.offset)
        lastSwitchingData?.let { layoutView(it, 0, 0, offsetX, offsetY, lastAlpha) }
    }

    private fun layoutView(switchingData: SwitchingData, startX: Int, startY: Int, offsetX: Int, offsetY: Int, alpha: Float) {
        val view = switchingData.view ?: return
        val rect = switchingData.viewRect ?: return
        val x = startX + offsetX + paddingLeft
        val y = startY + offsetY + paddingTop
        view.alpha = alpha
        view.layout(rect.left + x, rect.top + y, rect.right + x, rect.bottom + y)
    }

    fun hideView(toSide: Side = Side.BOTTOM, animate: Boolean = true) {
        val fromSide = when (toSide) {
            Side.LEFT -> Side.RIGHT
            Side.TOP -> Side.BOTTOM
            Side.RIGHT -> Side.LEFT
            Side.BOTTOM -> Side.TOP
        }
        showView(null, fromSide, animate)
    }

    fun showView(view: View?, fromSide: Side = Side.BOTTOM, animate: Boolean = initialized) {
        if (view == currentSwitchingData?.view) {
            return
        }
        nextSwitchingData = SwitchingData(fromSide, view, animate, null)
        checkAndTryShowNext()
    }

    private fun updateViewRect(switchingData: SwitchingData?, width: Int, height: Int) {
        val view = switchingData?.view ?: return
        switchingData.viewRect = layoutManager.layoutView(view, width, height)
    }

    private fun checkAndTryShowNext(): Boolean {
        if (offset < 1f) {
            return false
        }
        tryShowNext()
        return true
    }

    private fun tryShowNext() {
        val nextSwitchingData = synchronized(this, {
            val res = this.nextSwitchingData
            if (res == null) {
                onAnimationFinished()
                return
            }
            this.nextSwitchingData = null
            res
        })

        updating = true
        offset = 0f
        lastSwitchingData = currentSwitchingData
        currentSwitchingData = nextSwitchingData
        currentSwitchingData?.view?.let { addView(it) }
        updating = false

        if (currentSwitchingData?.animate ?: true) {
            animating = true
            Animator.doAnimation(animationTime, { offset = it }, INTERPOLATOR, {
                animating = false
                lastSwitchingData?.view?.let { removeView(it) }
                lastSwitchingData = null
                checkAndTryShowNext()
            })
        } else {
            offset = 1f
            lastSwitchingData?.view?.let { removeView(it) }
            lastSwitchingData = null
            checkAndTryShowNext()
        }
    }

    override fun addView(child: View?, index: Int) {
        (0 until childCount).forEach {
            if (getChildAt(it) == child) {
                return
            }
        }
        super.addView(child, index)
    }

    protected open fun onAnimationFinished() {}

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        initialized = true

        val width = UiUtils.getMaxSize(0, widthMeasureSpec)
        val height = UiUtils.getDefaultSize(0, heightMeasureSpec)

        val widthWithPaddings = width - paddingLeft - paddingRight
        val heightWithPaddings = height - paddingTop - paddingBottom
        val lastSize = measureSwitchingData(lastSwitchingData, widthWithPaddings, heightWithPaddings)
        val currentSize = measureSwitchingData(currentSwitchingData, widthWithPaddings, heightWithPaddings)

        val viewWidth = lastSize.x + ((currentSize.x - lastSize.x) * offset).toInt()
        val viewHeight = lastSize.y + ((currentSize.y - lastSize.y) * offset).toInt()

        setMeasuredDimension(
                Math.max(width, viewWidth),
                Math.max(height, viewHeight)
        )
    }

    private fun measureSwitchingData(switchingData: SwitchingData?, width: Int, height: Int): Point {
        updateViewRect(switchingData, width, height)

        val view = switchingData?.view ?: return Point()
        val rect = switchingData.viewRect ?: return Point()

        val viewWidth = MeasureSpec.makeMeasureSpec(rect.width(), MeasureSpec.EXACTLY)
        val viewHeight = MeasureSpec.makeMeasureSpec(rect.height(), MeasureSpec.EXACTLY)

        view.measure(viewWidth, viewHeight)
        return Point(rect.width(), rect.height())
    }


}
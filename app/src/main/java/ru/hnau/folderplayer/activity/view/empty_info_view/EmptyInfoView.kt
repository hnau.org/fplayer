package ru.hnau.folderplayer.activity.view.empty_info_view

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.hnau.folderplayer.activity.view.button.TextButton
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.ui.ScreenManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPxInt
import kotlin.math.min


class EmptyInfoView(
        context: Context,
        title: StringGetter,
        subtitle: StringGetter? = null,
        buttonText: StringGetter? = null,
        buttonOnClickListener: (() -> Unit)? = null,
        buttonAccent: Boolean = false
) : ViewGroup(context) {

    companion object {

        private val CONTENT_WIDTH = (ScreenManager.sizePx.x * 0.75f).toInt()
        private val CONTENT_HEIGHT = (ScreenManager.sizePx.y * 0.5f).toInt()

    }

    private val titleView = Label(
            context = context,
            text = title,
            color = ColorGetter.FG,
            gravity = Gravity.CENTER,
            ellipsize = TextUtils.TruncateAt.END,
            size = SizeGetter(19)
    ).apply {
        setPadding(0, 0, 0, dpToPxInt(8))
    }

    private val subtitleView = subtitle?.let { subtitle ->
        Label(
                context = context,
                text = subtitle,
                color = ColorGetter.FG_TRANSPARENT_50,
                gravity = Gravity.CENTER,
                ellipsize = TextUtils.TruncateAt.END,
                size = SizeGetter(15)
        ).apply {
            setPadding(0, 0, 0, dpToPxInt(24))
        }
    }

    private val button = buttonText?.let { buttonText ->
        buttonOnClickListener?.let { buttonOnClickListener ->
            TextButton(
                    context = context,
                    size = TextButton.Size.MIDDLE,
                    color = if (buttonAccent) TextButton.Color.ACCENT else TextButton.Color.NORMAL,
                    text = buttonText,
                    onClickListener = buttonOnClickListener
            ).apply {
                layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT)
            }
        }
    }

    private val contentContainer = LinearLayout(context).apply {
        orientation = LinearLayout.VERTICAL
        addView(titleView)
        subtitleView?.let { addView(subtitleView) }
        button?.let { addView(button) }
    }

    init {
        addView(contentContainer)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val contentWidth = contentContainer.measuredWidth
        val contentHeight = contentContainer.measuredHeight
        val paddingH = (width - contentWidth) / 2
        val paddingV = (height - contentHeight) / 2
        contentContainer.layout(paddingH, paddingV, width - paddingH, height - paddingV)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val maxWidth = UiUtils.getMaxSize(0, widthMeasureSpec)
        val maxHeight = UiUtils.getMaxSize(0, heightMeasureSpec)

        contentContainer.measure(
                MeasureSpec.makeMeasureSpec(min(CONTENT_WIDTH, maxWidth), MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(min(CONTENT_HEIGHT, maxHeight), MeasureSpec.AT_MOST)
        )

        setMeasuredDimension(maxWidth, maxHeight)
    }

}
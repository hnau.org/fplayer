package ru.hnau.folderplayer.activity.view.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


abstract class BaseListAdapter<T>: RecyclerView.Adapter<BaseListAdapter.BaseListViewHolder>() {

    override fun onBindViewHolder(holder: BaseListViewHolder, position: Int) {
        val view = holder.content
        val type = holder.type
        val item = getItem(position)
        bindItem(view, item, type)
    }

    override fun getItemViewType(position: Int) = getItemType(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = BaseListViewHolder(generateView(viewType), viewType)

    open fun getItemType(position: Int) = 0

    abstract fun getItem(position: Int): T

    abstract fun bindItem(view: View, item: T, type: Int)

    abstract fun generateView(type: Int): View

    class BaseListViewHolder(val content: View, val type: Int) : RecyclerView.ViewHolder(content)

}
package ru.hnau.folderplayer.activity.screen_pages.settings

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.screen_pages.settings.item.*
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise.SettingsItemChoiceProperty
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise.SettingsItemChoiceViewItem
import ru.hnau.folderplayer.activity.view.list.BaseList
import ru.hnau.folderplayer.activity.view.list.BaseListAdapter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.managers.audio.ResumeWhereStoppedManager
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager
import ru.hnau.folderplayer.utils.ui.ScaleManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.widget.WidgetWideProvider


class SettingsList(context: Context) : BaseList<SettingsItem>(context, LayoutType.VERTICAL) {

    private data class ScaleTypeChoiceItem(val id: Int) : SettingsItemChoiceViewItem {
        override fun getDescription() = Math.round((ScaleManager.values[id]
                ?: 1f) * 100).toString() + "%"
    }

    companion object {

        private val scaleTypeChoiceItems: Map<Int, ScaleTypeChoiceItem> = ScaleManager.ids.associate {
            Pair(it, ScaleTypeChoiceItem(it))
        }

        private val items = arrayOf(
                SettingsItemTitle(StringGetter(R.string.settings_screen_page_group_interface_title)),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_go_up_on_back_clicked_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.goToUpDirOnBackClicked },
                                setter = { PreferencesManager.goToUpDirOnBackClicked = it }
                        )
                ),
                SettingsItemChoice(
                        title = StringGetter(R.string.settings_screen_page_interface_scale_title),
                        property = SettingsItemChoiceProperty(
                                getter = { scaleTypeChoiceItems[ScaleManager.id]!! },
                                setter = {
                                    (it as? ScaleTypeChoiceItem)?.let {
                                        ScaleManager.id = it.id
                                        AppActivityManager.reloadViews()
                                    }
                                }
                        ),
                        values = scaleTypeChoiceItems.values.toList()
                ),
                SettingsItemTitle(StringGetter(R.string.settings_screen_page_group_play_title)),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_pause_unplugged_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.pauseOnHeadphonesUnplugged },
                                setter = { PreferencesManager.pauseOnHeadphonesUnplugged = it }
                        )
                ),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_resume_plugged_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.resumeOnHeadphonesPlugged },
                                setter = { PreferencesManager.resumeOnHeadphonesPlugged = it }
                        )
                ),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_pause_unfocused_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.pauseOnUnfocused },
                                setter = { PreferencesManager.pauseOnUnfocused = it }
                        )
                ),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_resume_focused_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.resumeOnFocused },
                                setter = { PreferencesManager.resumeOnFocused = it }
                        )
                ),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_resume_where_stopped_title),
                        property = SettingsProperty(
                                getter = { ResumeWhereStoppedManager.resumeWhereStopped },
                                setter = { ResumeWhereStoppedManager.resumeWhereStopped = it }
                        )
                ),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_resume_where_stopped_inactive_widget_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.resumeWhereStoppedInactiveWidget },
                                setter = {
                                    PreferencesManager.resumeWhereStoppedInactiveWidget = it
                                    WidgetWideProvider.updateWidgets()
                                }
                        )
                ),
                SettingsItemBoolean(
                        title = StringGetter(R.string.settings_screen_page_pause_between_files_title),
                        property = SettingsProperty(
                                getter = { PreferencesManager.pauseBetweenFiles },
                                setter = {
                                    PreferencesManager.pauseBetweenFiles = it
                                }
                        )
                )
        )

    }

    private val settingsItemAdapter = object : BaseListAdapter<SettingsItem>() {
        override fun bindItem(view: View, item: SettingsItem, type: Int) = item.bindView(view)
        override fun generateView(type: Int) = SettingsItem.createView(context, type).apply {
            layoutParams = RecyclerView.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT).apply {
                bottomMargin = UiUtils.ELEMENTS_SEPARATION
            }
        }

        override fun getItemCount() = items.size
        override fun getItem(position: Int) = items[position]
        override fun getItemType(position: Int) = getItem(position).getViewType()
    }

    init {
        baseListAdapter = settingsItemAdapter
    }

}
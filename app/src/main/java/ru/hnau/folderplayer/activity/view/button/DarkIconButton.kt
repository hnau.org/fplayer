package ru.hnau.folderplayer.activity.view.button

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.View
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.ripple_animator.RippleAnimator
import ru.hnau.folderplayer.utils.ui.dpToPxInt
import ru.hnau.folderplayer.utils.ui.preferredHeight
import ru.hnau.folderplayer.utils.ui.preferredWidth


class DarkIconButton(
        context: Context,
        initialIcon: DrawableGetter,
        onClick: () -> Unit
) : View(context) {

    private val ICON_PADDING = dpToPxInt(8f)
    private val RIPPLE_COLOR = Color.BLACK

    var icon: DrawableGetter = initialIcon
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    var bgColor = ColorGetter.BG
    set(value) {
        if (field != value) {
            field = value
            bgPaint.color = bgColor
            invalidate()
        }
    }

    private val rippleAnimator = RippleAnimator(
            context = context,
            onNeedRefresh = this::invalidate,
            fingerCircleRadius = SizeGetter(30),
            circleDeltaRadius = SizeGetter(60),
            maxBgPercentage = 0.3f
    )

    private val bgPaint = Paint().apply {
        color = bgColor
    }

    private val ripplePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = RIPPLE_COLOR
    }


    private val drawable: Drawable
        get() = icon.get(context)

    private val drawableWidth = drawable.preferredWidth()
    private val drawableHeight = drawable.preferredHeight()

    init {
        setOnClickListener { onClick.invoke() }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val drawRect = Rect(
                paddingLeft,
                paddingTop,
                width - paddingRight,
                height - paddingBottom
        )
        canvas.save()
        canvas.clipRect(drawRect)

        canvas.drawRect(drawRect, bgPaint)


        ripplePaint.alpha = (255 * rippleAnimator.bgPercentage).toInt()
        canvas.drawRect(drawRect, ripplePaint)
        rippleAnimator.drawCircles { pos, r, alpha, _ ->
            ripplePaint.alpha = (255 * alpha).toInt()
            canvas.drawCircle(pos.x, pos.y, r, ripplePaint)
        }

        val cx = paddingLeft + (width - paddingLeft - paddingRight) / 2f
        val cy = paddingTop + (height - paddingTop - paddingBottom) / 2f
        val dx = drawableWidth / 2f
        val dy = drawableHeight / 2f

        drawable.bounds = Rect(
                (cx - dx).toInt(),
                (cy - dy).toInt(),
                (cx + dx).toInt(),
                (cy + dy).toInt()
        )
        drawable.draw(canvas)

        canvas.restore()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        rippleAnimator.onTouchEvent(event)
        super.onTouchEvent(event)
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = drawableWidth + ICON_PADDING * 2 + paddingLeft + paddingRight
        val height = drawableHeight + ICON_PADDING * 2 + paddingLeft + paddingRight
        setMeasuredDimension(
                UiUtils.getDefaultSize(width, widthMeasureSpec),
                UiUtils.getDefaultSize(height, heightMeasureSpec)
        )
    }


}
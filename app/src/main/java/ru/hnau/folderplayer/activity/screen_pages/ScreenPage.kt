package ru.hnau.folderplayer.activity.screen_pages

import android.content.Context
import android.view.View
import java.lang.ref.SoftReference

abstract class ScreenPage<out T : View>() {

    private var viewReference: SoftReference<T>? = null

    protected val view: T?
        get() = viewReference?.get()

    fun getView(context: Context): T {
        var view = this.view
        if (view == null) {
            view = createView(context)
            viewReference = SoftReference(view)
        }
        return view
    }

    fun removeCachedView() {
        viewReference?.clear()
    }

    protected abstract fun createView(context: Context): T

    open fun handleGoBack() = false

    open fun onShow() {}

    open fun onHide() {}

}
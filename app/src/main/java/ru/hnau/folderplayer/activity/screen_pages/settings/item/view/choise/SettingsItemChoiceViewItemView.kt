package ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.folderplayer.activity.screen_pages.settings.item.SettingsItem
import ru.hnau.folderplayer.activity.view.ClickableLinearLayout
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class SettingsItemChoiceViewItemView(
        context: Context,
        private val onClickListener: (SettingsItemChoiceViewItem) -> Unit,
        private val addOnSelectedStateChangedListenerCallback: (() -> Unit) -> Unit,
        private val removeOnSelectedStateChangedListenerCallback: (() -> Unit) -> Unit,
        private val selectedGetter: () -> SettingsItemChoiceViewItem?
) : ClickableLinearLayout(
        context
) {

    private val PADDING_V = dpToPxInt(8)
    private val TEXT_COLOR = ColorGetter.FG
    private val TEXT_SELECTED_COLOR = ColorGetter.PRIMARY
    private val BG_COLOR = ColorGetter.BG
    private val BG_SELECTED_COLOR = ColorGetter.SELECT

    private val titleView = Label(
            context = context,
            minLines = 1,
            maxLines = 1,
            size = SizeGetter(17)
    ).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, UiUtils.WRAP_CONTENT)
    }

    var item: SettingsItemChoiceViewItem? = null
        set(value) {
            if (field != value && value != null) {
                field = value
                titleView.text = value.getDescription()
                updateSelectedState()
            }
        }

    private var selectedState: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                onSelectedChanged(value)
            }
        }

    private val onSelectedStateChangedListener = this::updateSelectedState

    init {
        orientation = VERTICAL
        val paddingH = SettingsItem.PADDING_H
        setPadding(paddingH, PADDING_V, paddingH, PADDING_V)
        addView(titleView)

        setOnClickListener { item?.let { onClickListener.invoke(it) } }

        onSelectedChanged(false)

    }

    private fun updateSelectedState() {
        selectedState = selectedGetter.invoke() == item
    }

    private fun onSelectedChanged(selected: Boolean) {
        bgColor = if (selected) BG_SELECTED_COLOR else BG_COLOR
        titleView.setTextColor(if (selected) TEXT_SELECTED_COLOR else TEXT_COLOR)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addOnSelectedStateChangedListenerCallback.invoke(onSelectedStateChangedListener)
        updateSelectedState()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeOnSelectedStateChangedListenerCallback.invoke(onSelectedStateChangedListener)
    }

}
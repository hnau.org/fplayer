package ru.hnau.folderplayer.activity.view.header

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.button.DarkIconButton
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class Header(
        context: Context,
        title: StringGetter
) : LinearLayout(context) {

    private val backButton = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_header_back),
            onClick = { AppActivityManager.goBack() }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.APP_BAR_HEIGHT, UiUtils.APP_BAR_HEIGHT)
    }

    private val titleView = Label(
            context = context,
            text = StringGetter(title.get(context).toUpperCase()),
            color = ColorGetter.FG,
            minLines = 1,
            maxLines = 1,
            gravity = Gravity.CENTER_VERTICAL or Gravity.LEFT,
            size = SizeGetter(19),
            ellipsize = TextUtils.TruncateAt.END
    ).apply {
        val paddingH = dpToPxInt(16)
        setPadding(paddingH, 0, paddingH, 0)
        setBackgroundColor(ColorGetter.BG)
        layoutParams = LinearLayout.LayoutParams(0, UiUtils.MATCH_PARENT, 1f)
    }

    init {
        orientation = HORIZONTAL
        setPadding(0, 0, 0, UiUtils.ELEMENTS_SEPARATION)
        addView(backButton)
        addView(titleView)
    }

    fun addButton(icon: DrawableGetter, onClick: () -> Unit) {
        val button = DarkIconButton(
                context = context,
                initialIcon = icon,
                onClick = onClick
        ).apply {
            layoutParams = LinearLayout.LayoutParams(UiUtils.APP_BAR_HEIGHT, UiUtils.APP_BAR_HEIGHT)
        }
        addView(button)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(
                widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(UiUtils.ELEMENTS_SEPARATION + UiUtils.APP_BAR_HEIGHT, MeasureSpec.EXACTLY)
        )
    }

}
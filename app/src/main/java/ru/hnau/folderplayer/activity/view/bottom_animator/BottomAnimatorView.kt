package ru.hnau.folderplayer.activity.view.bottom_animator

import android.view.View


data class BottomAnimatorView(
        val view: View,
        val goBackHandler: () -> Boolean,
        val onHideCallback: (() -> Unit)? = null
)
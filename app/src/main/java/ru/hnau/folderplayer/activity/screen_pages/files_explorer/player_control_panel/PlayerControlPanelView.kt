package ru.hnau.folderplayer.activity.screen_pages.files_explorer.player_control_panel

import android.content.Context
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator


class PlayerControlPanelView(context: Context) : ViewGroup(context) {

    private var initialized = false

    private val innerView = PlayerControlPanelInnerView(context)

    private val visibilityAnimator = TwoStateAnimator().apply {
        iterator = { requestLayout() }
        interpolatorPositive = DecelerateInterpolator()
        interpolatorNegative = AccelerateInterpolator()
    }

    private val onStateChangedListener = { state: PlayerManager.State ->
        visibilityAnimator.addNewTarget(state.currentItem != null, !initialized)
    }

    init {
        addView(innerView)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        PlayerManager.addOnStateChangedListener(onStateChangedListener)
        onStateChangedListener.invoke(PlayerManager.state)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        PlayerManager.removeOnStateChangedListener(onStateChangedListener)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        innerView.layout(
                0,
                UiUtils.ELEMENTS_SEPARATION,
                innerView.measuredWidth,
                innerView.measuredHeight + UiUtils.ELEMENTS_SEPARATION
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        initialized = true
        innerView.measure(widthMeasureSpec, heightMeasureSpec)
        val width = innerView.measuredWidth
        val height = (innerView.measuredHeight * visibilityAnimator.currentValue).toInt() + UiUtils.ELEMENTS_SEPARATION
        setMeasuredDimension(width, height)

    }


}
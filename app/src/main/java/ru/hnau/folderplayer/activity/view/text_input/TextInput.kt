package ru.hnau.folderplayer.activity.view.text_input

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.TextUtils
import android.util.TypedValue
import android.view.Gravity
import android.widget.EditText
import ru.hnau.folderplayer.activity.view.label.TextViewInfo
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.getters.SizeGetter
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.ui.FontManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator
import ru.hnau.folderplayer.utils.ui.dpToPx
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class TextInput(
        context: Context,
        initialText: String = "",
        initialTitle: StringGetter = StringGetter(),
        viewInfo: TextViewInfo
) : EditText(context) {

    constructor(
            context: Context,
            initialText: String = "",
            initialTitle: StringGetter = StringGetter(),
            size: SizeGetter = SizeGetter(20),
            color: Int = ColorGetter.FG,
            gravity: Int = Gravity.CENTER_VERTICAL or Gravity.LEFT,
            maxLines: Int? = 1,
            minLines: Int? = 1,
            ellipsize: TextUtils.TruncateAt = TextUtils.TruncateAt.END
    ) : this(
            context = context,
            viewInfo = TextViewInfo(
                    size = size,
                    color = color,
                    gravity = gravity,
                    maxLines = maxLines,
                    minLines = minLines,
                    ellipsize = ellipsize
            ),
            initialText = initialText,
            initialTitle = initialTitle
    )

    companion object {
        private val TEXT_PADDING_V = dpToPxInt(8)
        private val TEXT_PADDING_H = dpToPxInt(8)
        private val PADDING_FOR_TITLE = dpToPxInt(24)
        private val TITLE_TEXT_SIZE = dpToPx(15)
        private val PADDING_TOP = TEXT_PADDING_V + PADDING_FOR_TITLE
        private val PADDING_BOTTOM = TEXT_PADDING_V
        private val TITLE_Y = dpToPxInt(2) + TITLE_TEXT_SIZE

        private val TITLE_COLOR = ColorGetter.FG_TRANSPARENT_50
        private val TITLE_COLOR_FOCUSED = ColorGetter.FG
        private val TEXT_COLOR = ColorGetter.FG
        private val TEXT_COLOR_FOCUSED = ColorGetter.PRIMARY
        private val BG_COLOR = ColorGetter.BG_DARK
        private val BG_COLOR_FOCUSED = ColorGetter.SELECT
    }

    private val focusAnimator = TwoStateAnimator().apply {
        iterator = this@TextInput::onFocusedValueChanged
        time = 200
    }

    var title: StringGetter = initialTitle
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    private val titlePaint = FontManager.getPaint().apply {
        textSize = TITLE_TEXT_SIZE
    }

    private val bgPaint = Paint()

    init {
        FontManager.fontView(this)
        setBackgroundColor(ColorGetter.TRANSPARENT)
        setTextSize(TypedValue.COMPLEX_UNIT_PX, viewInfo.size.get(context))
        setTextColor(viewInfo.color)
        gravity = viewInfo.gravity
        setSingleLine(viewInfo.maxLines == 1)
        viewInfo.maxLines?.let { this.maxLines = it }
        viewInfo.minLines?.let { this.minLines = it }
        ellipsize = viewInfo.ellipsize
        setText(initialText)
        setPadding(TEXT_PADDING_H, PADDING_TOP, TEXT_PADDING_H, PADDING_BOTTOM)
        onFocusedValueChanged(0f)
    }

    override fun draw(canvas: Canvas) {

        canvas.drawRect(Rect(0, PADDING_FOR_TITLE, width, height), bgPaint)

        canvas.save()
        canvas.clipRect(Rect(0, 0, width, PADDING_FOR_TITLE))
        canvas.drawText(title.get(context) + ":", 0f, TITLE_Y, titlePaint)
        canvas.restore()

        super.draw(canvas)
    }

    private fun onFocusedValueChanged(value: Float) {
        bgPaint.color = UiUtils.getColorInterTwoColors(BG_COLOR, BG_COLOR_FOCUSED, value)
        titlePaint.color = UiUtils.getColorInterTwoColors(TITLE_COLOR, TITLE_COLOR_FOCUSED, value)
        setTextColor(UiUtils.getColorInterTwoColors(TEXT_COLOR, TEXT_COLOR_FOCUSED, value))
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        focusAnimator.animateTo(focused)
    }

}
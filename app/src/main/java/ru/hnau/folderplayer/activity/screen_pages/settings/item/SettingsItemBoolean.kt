package ru.hnau.folderplayer.activity.screen_pages.settings.item

import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.SettingsItemBooleanView
import ru.hnau.folderplayer.utils.getters.StringGetter

class SettingsItemBoolean(
        private val title: StringGetter,
        private val property: SettingsProperty<Boolean>
) : SettingsItem({
    val view = it as? SettingsItemBooleanView
    if (view != null) {
        view.title = title
        view.property = property
    }
})
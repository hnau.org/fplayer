package ru.hnau.folderplayer.activity.view.collapsible_view

import android.graphics.Rect
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator
import kotlin.math.min


open class CollapsibleView(
        private val content: View,
        initExpanded: Boolean = false,
        private val maxContentHeight: Int? = null
) : ViewGroup(content.context) {

    private val collapsingAnimator = TwoStateAnimator(initExpanded).apply {
        time = 200
        iterator = { requestLayout() }
        interpolator = FastOutSlowInInterpolator()
    }

    init {
        addView(content)
    }

    fun collapse(animate: Boolean = true) = collapsingAnimator.addNewTarget(false, !animate)
    fun expand(animate: Boolean = true) = collapsingAnimator.addNewTarget(true, !animate)
    fun switch(animate: Boolean = true) = collapsingAnimator.animateOrSwitch(animate)

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val height = this.height
        val width = this.width
        val offset = (content.measuredHeight - height) / 2
        content.layout(0, -offset, width, height + offset)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val mode = MeasureSpec.getMode(heightMeasureSpec)
        when (mode) {
            MeasureSpec.EXACTLY -> {
                content.measure(widthMeasureSpec, heightMeasureSpec)
                setMeasuredDimension(content.measuredWidth, content.measuredHeight)
            }
            MeasureSpec.UNSPECIFIED -> {
                val measureSpecHeightForContent = if (maxContentHeight == null) {
                    heightMeasureSpec
                } else {
                    MeasureSpec.makeMeasureSpec(maxContentHeight, MeasureSpec.AT_MOST)
                }
                content.measure(widthMeasureSpec, measureSpecHeightForContent)
                val height = (content.measuredHeight * collapsingAnimator.currentValue).toInt()
                setMeasuredDimension(content.measuredWidth, height)
            }
            MeasureSpec.AT_MOST -> {
                content.measure(widthMeasureSpec, heightMeasureSpec)
                val height = if (maxContentHeight == null) content.measuredHeight else min(content.measuredHeight, maxContentHeight)
                setMeasuredDimension(content.measuredWidth, height)
            }
        }
    }


}
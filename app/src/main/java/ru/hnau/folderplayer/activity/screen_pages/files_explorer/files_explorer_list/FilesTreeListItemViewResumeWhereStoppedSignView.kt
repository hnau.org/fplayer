package ru.hnau.folderplayer.activity.screen_pages.files_explorer.files_explorer_list

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import ru.hnau.folderplayer.utils.Utils
import ru.hnau.folderplayer.utils.files_tree.item.FilesTreeItem
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.managers.audio.ResumeWhereStoppedManager
import ru.hnau.folderplayer.utils.ui.FontManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator
import ru.hnau.folderplayer.utils.ui.dpToPx
import ru.hnau.folderplayer.utils.ui.dpToPxInt

class FilesTreeListItemViewResumeWhereStoppedSignView(context: Context) : View(context) {

    private val TEXT_COLOR = ColorGetter.FG_TRANSPARENT_50
    private val TEXT_ACTIVE_COLOR = ColorGetter.PRIMARY
    private val BG_COLOR = ColorGetter.BG_DARK
    private val BG_ACTIVE_COLOR = ColorGetter.SELECT_DARK

    private val PADDING_H = dpToPxInt(8)
    private val PADDING_V = dpToPxInt(8)
    private val MARGIN_LEFT = dpToPxInt(8)

    private val TEXT_SIZE = dpToPx(15)

    private val BG_CORNER_RADIUS = dpToPxInt(4)

    var item: FilesTreeItem? = null
        set(value) {
            field = value
            resumeWhereStoppedStateChanged(false)
        }

    var itemIsPlaying: Boolean = false
        set(value) {
            field = value
            updateColors()
        }

    private val visibilityAnimator = TwoStateAnimator().apply {
        iterator = { requestLayout() }
        interpolator = FastOutSlowInInterpolator()
        time = 200
    }

    private val onResumeWhereStoppedStateChanged = { resumeWhereStoppedStateChanged(true) }

    private var text: String = ""

    private val bgPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint = FontManager.getPaint().apply {
        textAlign = Paint.Align.CENTER
        textSize = TEXT_SIZE
    }

    private val textHeight = -textPaint.fontMetrics.ascent

    private fun resumeWhereStoppedStateChanged(animate: Boolean) {
        val item = item ?: return
        val secondsPosition = ResumeWhereStoppedManager.getFilesTreeItemStartSecondsPosition(item)
        if (secondsPosition != null) {
            text = Utils.timeToStr(secondsPosition)
        }
        visibilityAnimator.addNewTarget(secondsPosition != null, !animate)
    }

    private fun updateColors() {
        bgPaint.color = if (itemIsPlaying) BG_ACTIVE_COLOR else BG_COLOR
        textPaint.color = if (itemIsPlaying) TEXT_ACTIVE_COLOR else TEXT_COLOR
        invalidate()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val bgPath = UiUtils.calcCardPath(MARGIN_LEFT, 0, width - MARGIN_LEFT, height, BG_CORNER_RADIUS)
        canvas.drawPath(bgPath, bgPaint)

        canvas.drawText(text, MARGIN_LEFT + (width - MARGIN_LEFT) / 2f, PADDING_V + textHeight * 0.85f, textPaint)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        ResumeWhereStoppedManager.addResumeWhereStoppedStateChangedListener(onResumeWhereStoppedStateChanged)
        resumeWhereStoppedStateChanged(false)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        ResumeWhereStoppedManager.removeResumeWhereStoppedStateChangedListener(onResumeWhereStoppedStateChanged)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = (textHeight + PADDING_V * 2).toInt()
        val width = ((textPaint.measureText(text) + PADDING_H * 2 + MARGIN_LEFT) * visibilityAnimator.currentValue).toInt()
        setMeasuredDimension(width, height)
    }


}
package ru.hnau.folderplayer.activity.screen_pages.settings.item

import android.view.View
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise.SettingsItemChoiceProperty
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise.SettingsItemChoiceView
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise.SettingsItemChoiceViewItem
import ru.hnau.folderplayer.utils.getters.StringGetter


class SettingsItemChoice<T: SettingsItemChoiceViewItem>(
        private val title: StringGetter,
        private val property: SettingsItemChoiceProperty,
        private val values: List<T>
) : SettingsItem({
    val view = (it as? SettingsItemChoiceView)
    if (view != null) {
        view.title = title
        view.selectedItemPropertyWithValues = Pair(property, values)
    }
})
package ru.hnau.folderplayer.activity.screen_pages

import android.content.Context
import android.view.View
import ru.hnau.folderplayer.activity.view.views_switcher.ViewsSwitcher
import ru.hnau.folderplayer.utils.getters.ColorGetter
import java.util.*

class ScreenPagesContainer(
        context: Context,
        initialPage: ScreenPage<*>
) : ViewsSwitcher(context) {

    private val pagesStack = Stack<ScreenPage<*>>()

    private val currentPage: ScreenPage<*>?
        get() = if (pagesStack.empty()) null else pagesStack.peek()

    private val currentPageView: View?
        get() = currentPage?.getView(context)

    init {
        setBackgroundColor(ColorGetter.BG_DARK)
        show(initialPage, false)
    }

    fun show(page: ScreenPage<*>) = show(page, true)

    private fun show(page: ScreenPage<*>, animate: Boolean) {
        currentPage?.onHide()
        pagesStack.push(page)
        currentPage?.onShow()
        showView(currentPageView, Side.RIGHT, animate)
    }

    fun handleGoBack(force: Boolean = false): Boolean {
        if (!force && currentPage?.handleGoBack() == true) {
            return true
        }

        if (pagesStack.size == 1) {
            return false
        }

        goBack()
        return true
    }

    fun reloadViews() {
        pagesStack.forEach { it.removeCachedView() }
        showView(currentPageView, Side.LEFT, false)
    }

    private fun goBack() {
        currentPage?.onHide()
        pagesStack.pop()
        currentPage?.onShow()
        showView(currentPageView, Side.LEFT, true)
    }

}
package ru.hnau.folderplayer.activity.screen_pages.files_explorer

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.screen_pages.equalizer.EqualizerScreenPage
import ru.hnau.folderplayer.activity.screen_pages.settings.SettingsScreenPage
import ru.hnau.folderplayer.activity.view.button.DarkIconButton
import ru.hnau.folderplayer.utils.getters.DrawableGetter
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.ui.UiUtils


class FilesExplorerAppBar(
        context: Context,
        onBackClicked: () -> Unit,
        onGoToCurrentDirClicked: () -> Unit,
        onGoToBookmarksClicked: () -> Unit,
        onGoHomeClicked: () -> Unit
) : LinearLayout(context) {

    private val BUTTON_HEIGHT = UiUtils.APP_BAR_HEIGHT

    private val buttonBack = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_files_explorer_app_bar_back),
            onClick = {
                onBackClicked.invoke()
                FirebaseManager.sendEvent("Files explorer app bar button clicked", mapOf("type" to "back"))
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val buttonHome = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_files_explorer_app_bar_home),
            onClick = {
                onGoHomeClicked.invoke()
                FirebaseManager.sendEvent("Files explorer app bar button clicked", mapOf("type" to "home"))
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val buttonBookmarks = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_files_explorer_app_bar_bookmarks),
            onClick = {
                onGoToBookmarksClicked.invoke()
                FirebaseManager.sendEvent("Files explorer app bar button clicked", mapOf("type" to "bookmarks"))
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val buttonEqualizer = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_files_explorer_app_bar_equalizer),
            onClick = {
                goToEqualizer()
                FirebaseManager.sendEvent("Files explorer app bar button clicked", mapOf("type" to "equalizer"))
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val buttonSettings = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_files_explorer_app_bar_settings),
            onClick = {
                goToSettings()
                FirebaseManager.sendEvent("Files explorer app bar button clicked", mapOf("type" to "settings"))
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    private val buttonCurrentDir = DarkIconButton(
            context = context,
            initialIcon = DrawableGetter(R.drawable.ic_files_explorer_app_bar_to_current_dir),
            onClick = {
                onGoToCurrentDirClicked.invoke()
                FirebaseManager.sendEvent("Files explorer app bar button clicked", mapOf("type" to "current dir"))
            }
    ).apply {
        layoutParams = LinearLayout.LayoutParams(0, BUTTON_HEIGHT, 1f)
    }

    init {
        orientation = HORIZONTAL
        addView(buttonBack)
        addView(buttonHome)
        addView(buttonBookmarks)
        addView(buttonEqualizer)
        addView(buttonSettings)
        addView(buttonCurrentDir)
    }

    private fun goToEqualizer() {
        AppActivityManager.showScreenPage(EqualizerScreenPage())
    }

    private fun goToSettings() {
        AppActivityManager.showScreenPage(SettingsScreenPage())
    }

}
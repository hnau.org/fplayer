package ru.hnau.folderplayer.activity.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.widget.LinearLayout
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.ui.animations.ripple_animator.RippleAnimator


open class ClickableLinearLayout(
        context: Context,
        initialBgColor: Int = ColorGetter.BG,
        initialRippleColor: Int = ColorGetter.BG_DARK
) : LinearLayout(context) {


    protected var bgColor = initialBgColor
        set(value) {
            field = value
            invalidate()
        }

    protected var rippleColor = initialRippleColor
        set(value) {
            field = value
            invalidate()
        }

    private val rippleAnimator = RippleAnimator(
            context = context,
            onNeedRefresh = this::invalidate,
            maxBgPercentage = 0.5f
    )

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)


    override fun dispatchDraw(canvas: Canvas) {

        val fullRect = Rect(0, 0, width, height)

        paint.color = bgColor
        canvas.drawRect(fullRect, paint)


        paint.color = rippleColor
        paint.alpha = (rippleAnimator.bgPercentage * 255).toInt()
        canvas.drawRect(fullRect, paint)

        rippleAnimator.drawCircles { pos, r, alpha, _ ->
            paint.alpha = (255 * alpha).toInt()
            canvas.drawCircle(pos.x, pos.y, r, paint)
        }

        super.dispatchDraw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        rippleAnimator.onTouchEvent(event)
        super.onTouchEvent(event)
        return true
    }

}
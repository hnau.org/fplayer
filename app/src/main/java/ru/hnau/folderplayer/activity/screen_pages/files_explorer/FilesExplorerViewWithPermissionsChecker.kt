package ru.hnau.folderplayer.activity.screen_pages.files_explorer

import android.content.Context
import android.view.View
import ru.hnau.folderplayer.R
import ru.hnau.folderplayer.activity.view.empty_info_view.EmptyInfoView
import ru.hnau.folderplayer.activity.view.views_switcher.ViewsSwitcher
import ru.hnau.folderplayer.utils.getters.StringGetter
import ru.hnau.folderplayer.utils.managers.AppActivityManager
import ru.hnau.folderplayer.utils.permission.PermissionRequester
import ru.hnau.folderplayer.utils.permission.PermissionType


class FilesExplorerViewWithPermissionsChecker(context: Context) : ViewsSwitcher(context) {

    companion object {

        private val PAUSE_BEFORE_ANIMATION = 1000L // 1sec

    }

    private val createdTime = System.currentTimeMillis()

    private val permissionDeniedInfo: View by lazy {
        EmptyInfoView(
                context = context,
                title = StringGetter(R.string.external_storage_permission_denied_info_title),
                subtitle = StringGetter(R.string.external_storage_permission_denied_info_subtitle),
                buttonText = StringGetter(R.string.external_storage_permission_denied_info_button),
                buttonOnClickListener = this::requestPermission
        )
    }

    private val filesExplorerView: FilesExplorerView by lazy { FilesExplorerView(context) }

    init {
        requestPermission()
    }

    private fun requestPermission() {
        val activity = AppActivityManager.activity ?: return
        PermissionRequester.requestPermission(activity, PermissionType.WRITE_EXTERNAL_STORAGE, {
            showView(filesExplorerView)
        }, {
            showView(permissionDeniedInfo)
        })
    }

    private fun showView(view: View) {
        val animate = System.currentTimeMillis() - createdTime > PAUSE_BEFORE_ANIMATION
        showView(view, Side.RIGHT, animate)
    }

    fun handleGoBack() = (currentView as? FilesExplorerView)?.handleGoBack() ?: false

}
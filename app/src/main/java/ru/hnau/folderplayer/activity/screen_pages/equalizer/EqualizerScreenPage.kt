package ru.hnau.folderplayer.activity.screen_pages.equalizer

import android.content.Context
import ru.hnau.folderplayer.activity.screen_pages.ScreenPage
import ru.hnau.folderplayer.utils.managers.FirebaseManager
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager


class EqualizerScreenPage : ScreenPage<EqualizerScreenPageView>() {

    override fun createView(context: Context) = EqualizerScreenPageView(context)

    override fun onHide() {
        super.onHide()
        EqualizerManager.saveAllSchemas()
        val schemeName = EqualizerManager.getSchemeName(EqualizerManager.currentSchemeId)
        FirebaseManager.sendEvent("On equalizer scheme selected", mapOf("name" to schemeName))
    }

}
package ru.hnau.folderplayer.activity.screen_pages.equalizer.list

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.view.View
import ru.hnau.folderplayer.activity.screen_pages.equalizer.BezierPath
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.dpToPx
import ru.hnau.folderplayer.utils.ui.dpToPxInt


class EqualizerSchemasListItemIconView(context: Context) : View(context) {

    companion object {
        private val LINE_COLOR = ColorGetter.FG
        private val LINE_COLOR_ACTIVE = ColorGetter.PRIMARY
    }

    private val LINE_WIDTH = dpToPx(2)
    private val PREFERRED_SIZE = dpToPxInt(32)

    private val PADDING_H = dpToPxInt(2)
    private val PADDING_V = dpToPxInt(4)

    var id: Long = -1
        set(value) {
            if (field != value) {
                val oldField = field
                field = value
                onSchemeIdChanged(oldField, value)
            }
        }

    var active: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }


    private val onBandChangedListener: (Int) -> Unit = { invalidate() }

    private var attachedToWindow = false

    private val linePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeWidth = LINE_WIDTH
        strokeCap = Paint.Cap.ROUND
        strokeJoin = Paint.Join.ROUND
    }

    init {
        id = EqualizerManager.currentSchemeId
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val bandsCount = EqualizerManager.bandsCount
        if (bandsCount < 2) {
            return
        }

        val left = PADDING_H
        val top = PADDING_V
        val width = this.width - PADDING_H * 2
        val height = this.height - PADDING_V * 2

        val bandsD = width.toFloat() / (bandsCount - 1).toFloat()

        val points = (0 until bandsCount).map { band ->
            val x = left + (bandsD * band)
            val bandValue = EqualizerManager.getSchemeValue(id, band)
            val value = 1 - (bandValue + 1f) / 2
            val y = top + height * value
            PointF(x, y)
        }

        val linePath = BezierPath(points)
        linePaint.color = if (active) LINE_COLOR_ACTIVE else LINE_COLOR
        canvas.drawPath(linePath, linePaint)
    }

    private fun onSchemeIdChanged(oldId: Long, id: Long) {
        EqualizerManager.removeOnSchemeBandChangedListener(oldId, onBandChangedListener)
        if (attachedToWindow) {
            EqualizerManager.addOnSchemeBandChangedListener(id, onBandChangedListener)
        }
        invalidate()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        attachedToWindow = true
        EqualizerManager.addOnSchemeBandChangedListener(id, onBandChangedListener)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        attachedToWindow = false
        EqualizerManager.removeOnSchemeBandChangedListener(id, onBandChangedListener)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
                UiUtils.getDefaultSize(PREFERRED_SIZE, widthMeasureSpec),
                UiUtils.getDefaultSize(PREFERRED_SIZE, heightMeasureSpec)
        )
    }

}
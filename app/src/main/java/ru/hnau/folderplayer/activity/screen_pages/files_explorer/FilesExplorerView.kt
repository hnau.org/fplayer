package ru.hnau.folderplayer.activity.screen_pages.files_explorer

import android.content.Context
import android.widget.LinearLayout
import ru.hnau.folderplayer.activity.screen_pages.files_explorer.player_control_panel.PlayerControlPanelView
import ru.hnau.folderplayer.utils.files_tree.FileTreeItemStorageStringConverter
import ru.hnau.folderplayer.utils.files_tree.item.BookmarksFilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.OpenableFilesTreeItem
import ru.hnau.folderplayer.utils.managers.preferences.PreferencesManager
import ru.hnau.folderplayer.utils.ui.UiUtils


class FilesExplorerView(context: Context) : LinearLayout(context) {

    private val filesExplorer = FilesListsStackView(context).apply {
        layoutParams = LinearLayout.LayoutParams(UiUtils.MATCH_PARENT, 0, 1f)
    }

    private val appBarView = FilesExplorerAppBar(
            context = context,
            onBackClicked = { filesExplorer.handleGoBack() },
            onGoHomeClicked = filesExplorer::goHome,
            onGoToBookmarksClicked = { filesExplorer.openOpenableItem(BookmarksFilesTreeItem(context)) },
            onGoToCurrentDirClicked = this::goToCurrentDir
    ).apply {
        setPadding(0, 0, 0, UiUtils.ELEMENTS_SEPARATION)
    }

    private val playControlPanel = PlayerControlPanelView(context)

    init {
        orientation = VERTICAL
        addView(appBarView)
        addView(filesExplorer)
        addView(playControlPanel)
    }

    fun handleGoBack() = filesExplorer.handleGoBack()

    private fun goToCurrentDir() {
        val currentFilesTreeItem = FileTreeItemStorageStringConverter.getFilesTreeItem(context, PreferencesManager.filesExplorerViewPath) as? OpenableFilesTreeItem ?: return
        filesExplorer.openOpenableItem(currentFilesTreeItem)
    }
}
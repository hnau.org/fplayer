package ru.hnau.folderplayer.activity.screen_pages.files_explorer.player_control_panel

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator
import ru.hnau.folderplayer.utils.getters.ColorGetter
import ru.hnau.folderplayer.utils.managers.audio.PlayerManager
import ru.hnau.folderplayer.utils.managers.audio.player_session_manager.PlayerSessionManager
import ru.hnau.folderplayer.utils.ui.UiUtils
import ru.hnau.folderplayer.utils.ui.animations.TwoStateAnimator
import ru.hnau.folderplayer.utils.ui.dpToPx


class PlayerControlPanelProgressView(
        context: Context,
        private val onPercentageScrolledListener: (Float) -> Unit,
        private val onPercentageSelectedListener: (Float) -> Unit
) : View(
        context
) {

    companion object {

        private val LINE_COLOR = ColorGetter.FG_TRANSPARENT_50
        private val LINE_COLOR_ACTIVE = ColorGetter.PRIMARY
        private val BG_COLOR = ColorGetter.BG_DARK
        private val BG_COLOR_ACTIVE = ColorGetter.SELECT_DARK
        private val CIRCLE_COLOR = ColorGetter.FG
    }

    private val CIRCLE_R = dpToPx(8).toInt()
    private val LINE_HEIGHT_DIV_2 = dpToPx(4).toInt()

    private val activeAnimator = TwoStateAnimator().apply {
        time = 150
        interpolator = LinearInterpolator()
        iterator = { this@PlayerControlPanelProgressView.invalidate() }
    }

    var progress: Float = 0f
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    private val onStateChangedListener = { state: PlayerManager.State ->
        activeAnimator.addNewTarget(!state.paused, false)
    }

    private val minX: Int
        get() = paddingLeft + LINE_HEIGHT_DIV_2

    private val maxX: Int
        get() = width - paddingRight - LINE_HEIGHT_DIV_2

    private val progressX: Float
        get() = minX + (maxX - minX) * progress

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        val cy = paddingTop + (height - paddingTop - paddingBottom) / 2

        val lineLeft = paddingLeft
        val lineWidth = width - paddingRight - paddingLeft
        val linePath = UiUtils.calcRoundSidesPath(lineLeft, cy - LINE_HEIGHT_DIV_2, lineWidth, LINE_HEIGHT_DIV_2 * 2)

        val circleX = progressX.toInt()


        val activeLinePartRect = Rect(0, 0, circleX, height)
        canvas.save()
        canvas.clipRect(activeLinePartRect)
        val lineColor = UiUtils.getColorInterTwoColors(LINE_COLOR, LINE_COLOR_ACTIVE, activeAnimator.currentValue)
        paint.color = lineColor
        canvas.drawPath(linePath, paint)
        canvas.restore()

        val inactiveLinePartRect = Rect(circleX, 0, width, height)
        canvas.save()
        canvas.clipRect(inactiveLinePartRect)
        val bgColor = UiUtils.getColorInterTwoColors(BG_COLOR, BG_COLOR_ACTIVE, activeAnimator.currentValue)
        paint.color = bgColor
        canvas.drawPath(linePath, paint)
        canvas.restore()

        val circleR = CIRCLE_R.toFloat()
        paint.color = CIRCLE_COLOR
        canvas.drawCircle(circleX.toFloat(), cy.toFloat(), circleR, paint)

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val y = event.y
        val sizeD = (maxX - minX).toFloat()
        if (event.action != MotionEvent.ACTION_UP || sizeD <= 0 || y < 0 || y > height) {
            return true
        }

        val percentageRaw = (event.x - minX) / sizeD
        val percentage = if (percentageRaw < 0) 0f else if (percentageRaw > 1) 1f else percentageRaw
        onPercentageSelectedListener.invoke(percentage)
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val preferredHeight = CIRCLE_R * 2 + paddingTop + paddingBottom
        val width = UiUtils.getMaxSize(0, widthMeasureSpec)
        val height = UiUtils.getDefaultSize(preferredHeight, heightMeasureSpec)

        setMeasuredDimension(width, height)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        PlayerManager.addOnStateChangedListener(onStateChangedListener)
        onStateChangedListener.invoke(PlayerManager.state)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        PlayerManager.removeOnStateChangedListener(onStateChangedListener)
    }

}
package ru.hnau.folderplayer.activity.screen_pages.equalizer.list

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.hnau.folderplayer.activity.view.list.BaseListAdapter
import ru.hnau.folderplayer.utils.findPos
import ru.hnau.folderplayer.utils.managers.audio.equalizer.EqualizerManager
import ru.hnau.folderplayer.utils.ui.UiUtils


class EqualizerSchemasListAdapter(private val context: Context) : BaseListAdapter<Long>() {

    override fun getItem(position: Int) = EqualizerManager.schemasIds.getOrNull(position) ?: 0L

    override fun getItemCount() = EqualizerManager.schemasIds.size

    override fun bindItem(view: View, item: Long, type: Int) {
        (view as? EqualizerSchemasListItemView)?.id = item
    }

    override fun generateView(type: Int) = EqualizerSchemasListItemView(context).apply {
        layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
            bottomMargin = UiUtils.ELEMENTS_SEPARATION
        }
    }

    fun onSchemeAdded(id: Long) {
        val pos = getPositionById(EqualizerManager.schemasIds, id) ?: return
        notifyItemInserted(pos)
    }

    fun onSchemeRemoved(oldIdsAndRemovedId: Pair<List<Long>, Long>) {
        val pos = getPositionById(oldIdsAndRemovedId.first, oldIdsAndRemovedId.second) ?: return
        notifyItemRemoved(pos)
    }

    private fun getPositionById(ids: List<Long>, id: Long) = ids.findPos { it == id }

}
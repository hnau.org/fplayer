package ru.hnau.folderplayer.activity.view.views_switcher.layout_manager

import android.graphics.Rect
import android.view.Gravity
import android.view.View

/**
 * Определение размера и положения прокручиваемого элемента внутри ViewsSwitcher на основе View.LayoutParams
 */
class SimpleViewsSwitcherLayoutManager(val gravity: Int = Gravity.CENTER) : ViewsSwitcherLayoutManager {

    override fun layoutView(view: View, width: Int, height: Int): Rect {

        val layoutParams = view.layoutParams
        val layoutParamsWidth = layoutParams?.width ?: 0
        val layoutParamsHeight = layoutParams?.height ?: 0
        val viewWidth = if (layoutParamsWidth <= 0) width else layoutParamsWidth
        val viewHeight = if (layoutParamsHeight <= 0) height else layoutParamsHeight

        val res = Rect()
        Gravity.apply(gravity, viewWidth, viewHeight, Rect(0, 0, width, height), res)
        return res
    }

}
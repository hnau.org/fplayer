package ru.hnau.folderplayer.activity.screen_pages.settings.item

import android.content.Context
import android.view.View
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.SettingsItemBooleanView
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.SettingsItemTitleView
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.choise.SettingsItemChoiceView
import ru.hnau.folderplayer.utils.ui.dpToPxInt


abstract class SettingsItem(
        private val viewBinder: (View) -> Unit
) {

    fun bindView(view: View) = viewBinder.invoke(view)

    fun getViewType() = when (this) {
        is SettingsItemBoolean -> 1
        is SettingsItemChoice<*> -> 2
        else -> 0
    }

    companion object {

        val PADDING_V: Int
            get() = dpToPxInt(14)

        val PADDING_H: Int
            get() = dpToPxInt(16)

        fun createView(context: Context, itemType: Int) = when (itemType) {
            1 -> SettingsItemBooleanView(context)
            2 -> SettingsItemChoiceView(context)
            else -> SettingsItemTitleView(context)
        }

    }

}
package ru.hnau.folderplayer.activity.screen_pages.files_explorer.files_explorer_list

import android.content.Context
import ru.hnau.folderplayer.activity.view.list.BaseList
import ru.hnau.folderplayer.utils.files_tree.item.FilesTreeItem
import ru.hnau.folderplayer.utils.files_tree.item.OpenableFilesTreeItem


class FilesTreeList(
        context: Context,
        baseItem: OpenableFilesTreeItem,
        onItemClickCallback: (FilesTreeItem) -> Unit
) : BaseList<FilesTreeItem>(
        context,
        LayoutType.VERTICAL
) {

    private val adapter = FilesTreeListAdapter(this, baseItem, onItemClickCallback)

    init {
        baseListAdapter = adapter
    }

}
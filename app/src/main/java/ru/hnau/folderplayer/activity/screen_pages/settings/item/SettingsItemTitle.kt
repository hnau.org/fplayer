package ru.hnau.folderplayer.activity.screen_pages.settings.item

import android.content.Context
import android.view.View
import ru.hnau.folderplayer.activity.screen_pages.settings.item.view.SettingsItemTitleView
import ru.hnau.folderplayer.activity.view.label.Label
import ru.hnau.folderplayer.utils.getters.StringGetter


class SettingsItemTitle(
        private val title: StringGetter
) : SettingsItem(
        {
            (it as? SettingsItemTitleView)?.text = title.get(it.context)
        }
)
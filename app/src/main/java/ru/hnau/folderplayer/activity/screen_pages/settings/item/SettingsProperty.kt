package ru.hnau.folderplayer.activity.screen_pages.settings.item


data class SettingsProperty<T>(
        private val getter: () -> T,
        private val setter: (T) -> Unit
) {

    fun get() = getter.invoke()

    fun set(value: T) = setter.invoke(value)

}